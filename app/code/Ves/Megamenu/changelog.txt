= 1.1.9 - 2021-02-02 =
* Optimize - Category loading and searching
= 1.1.9 - 2021-07-19 =
* Fix issue with breadcrumbs in product detail page

= 1.1.9 - 2021-08-06 =
* Fix issue with button choose image to insert menu item icon, hover icon
* Fix issue with button open WYSIWYG Editor popup