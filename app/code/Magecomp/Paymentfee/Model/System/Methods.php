<?php

namespace Magecomp\Paymentfee\Model\System;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Config;
class Methods implements \Magento\Framework\Option\ArrayInterface
{
    protected $scopeConfig;
    protected $paymentmodelconfig;
    protected $paymentMethodList;
    protected $_storeManager;

    public function __construct(Config $paymentmodelconfig, ScopeConfigInterface $scopeConfig, \Magento\Payment\Model\PaymentMethodList $paymentMethodList, \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->paymentmodelconfig = $paymentmodelconfig;
        $this->scopeConfig = $scopeConfig;
        $this->paymentMethodList = $paymentMethodList;
        $this->_storeManager = $storeManager;
    }

    public function toOptionArray()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $paymentConfig = $objectManager->get('Magento\Payment\Model\PaymentMethodList');
        $scopeConfigInterface = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');

       //get all active payment methods
        //$storeId = $this->_storeManager->getStore()->getId();
        $storeId = 1;
        $activePaymentMethods = $this->paymentMethodList->getActiveList($storeId);
        $payments = $this->paymentmodelconfig->getActiveMethods();


        $methods = array();
        foreach ($activePaymentMethods as $paymentCode => $paymentModel)
        {
            $paymentCode = $paymentModel->getCode();
            $paymentTitle = $this->scopeConfig->getValue('payment/'.$paymentCode.'/title');
          //  $logger->info('Your text message'.$paymentTitle.$paymentCode );

            $methods[$paymentCode] = array(
                'label' => $paymentTitle,
                'value' => $paymentCode
            );
        }
        return $methods;
    }
}