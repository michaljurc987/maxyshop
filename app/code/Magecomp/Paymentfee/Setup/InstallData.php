<?php
namespace Magecomp\Paymentfee\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Sales\Setup\SalesSetup;
class InstallData implements InstallDataInterface
{
    private $salesSetup;
    private $quoteSetup;

    public function __construct(
        SalesSetup $salesSetup,
        QuoteSetup $quoteSetup
    ) {
        $this->salesSetup = $salesSetup;
        $this->quoteSetup = $quoteSetup;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // Quote Address attributes
        $this->quoteSetup->addAttribute('quote_address', 'mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->quoteSetup->addAttribute('quote_address', 'base_mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->quoteSetup->addAttribute('quote_address', 'mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);
        $this->quoteSetup->addAttribute('quote_address', 'base_mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);

		$this->quoteSetup->addAttribute('quote_address', 'mc_paymentfee_description', [
            'type'=>'varchar'
        ]);


        // Order attributes
        $this->salesSetup->addAttribute('order', 'mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('order', 'base_mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('order', 'mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('order', 'base_mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('order', 'mc_paymentfee_description', [
            'type'=>'varchar'
        ]);


        // Invoice attributes
        $this->salesSetup->addAttribute('invoice', 'mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('invoice', 'base_mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('invoice', 'mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('invoice', 'base_mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);


        // Creditmemo attributes
        $this->salesSetup->addAttribute('creditmemo', 'mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('creditmemo', 'base_mc_paymentfee_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('creditmemo', 'mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);
        $this->salesSetup->addAttribute('creditmemo', 'base_mc_paymentfee_tax_amount', [
            'type'=>'decimal'
        ]);


    }
}
