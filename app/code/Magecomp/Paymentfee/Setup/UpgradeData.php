<?php
namespace Magecomp\Paymentfee\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Sales\Setup\SalesSetup;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements  UpgradeDataInterface
{
    private $salesSetup;
    private $quoteSetup;

    public function __construct(
        SalesSetup $salesSetup,
        QuoteSetup $quoteSetup
    ) {
        $this->salesSetup = $salesSetup;
        $this->quoteSetup = $quoteSetup;
    }

    public function upgrade(ModuleDataSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->salesSetup->addAttribute('invoice', 'mc_paymentfee_description', [
            'type'=>'varchar'
        ]);

        $this->salesSetup->addAttribute('creditmemo', 'mc_paymentfee_description', [
            'type'=>'varchar'
        ]);
        $setup->endSetup();
    }
}