<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\PriceHistory\Ui\DataProvider\Listing\Collection;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Psr\Log\LoggerInterface as Logger;

class History extends SearchResult
{
    /**
     * HistoryResource constructor.
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable = 'history_price',
        $resourceModel = 'Mageside\PriceHistory\Model\ResourceModel\History'
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    /**
     * Init collection select
     *
     * @return $this
     */
    protected function _initSelect()
    {
        $this->getSelect()
            ->from(['main_table' => $this->getMainTable()])
            ->joinLeft(
                ['admin' => $this->getTable('admin_user')],
                'main_table.manager_id = admin.user_id',
                ['username' => 'admin.username']
            )
            ->join(
                ['ea' => $this->getTable('eav_attribute')],
                'main_table.attribute_id = ea.attribute_id',
                ['attribute_label' => 'ea.frontend_label']
            );

        $this->getSelect()->columns(
            [
                'difference' => new \Zend_Db_Expr('IF((new_price - old_price) > 0,
                concat(\'+\', CAST(new_price - old_price as char(15))),
                CAST(new_price - old_price as char(15))
                )'),
            ]
        );

        return $this;
    }

    public function addFieldToFilter($field, $condition = null)
    {
        if ($field == 'username') {
            $field = 'admin.username';
        } elseif ($field == 'attribute_label') {
            $field = 'ea.attribute_code';
        } else {
            $field = 'main_table.' . $field;
        }

        return parent::addFieldToFilter($field, $condition);
    }
}
