Magento 2 Price History by Mageside
===================================

####Support
    v1.0.9 - Magento 2.3.* - 2.4.*

####Change list
    v1.0.9 - Magento 2.4 support checking (updated composer.json)
    v1.0.8 - Fixed formatting price value
    v1.0.7 - Fixed error for duplicate products
    v1.0.6 - Magento 2.3 support checking (updated composer.json)
    v1.0.5 - Added Magento EE compatibility
    v1.0.4 - Fixed error for new products
    v1.0.2 - Magento 2.2 support checking (updated composer.json)
    v1.0.1 - Added acl config control
    v1.0.0 - Start project

####Installation
    1. Download the archive.
    2. Make sure to create the directory structure in your Magento - 'Magento_Root/app/code/Mageside/PriceHistory'.
    3. Unzip the content of archive to directory 'Magento_Root/app/code/Mageside/PriceHistory'
       (use command 'unzip ArchiveName.zip -d path_to/app/code/Mageside/PriceHistory').
    4. Run the command 'php bin/magento module:enable Mageside_PriceHistory' in Magento root.
       If you need to clear static content use 'php bin/magento module:enable --clear-static-content Mageside_PriceHistory'.
    5. Run the command 'php bin/magento setup:upgrade' in Magento root.
    6. Run the command 'php bin/magento setup:di:compile' if you have a single website and store, 
       or 'php bin/magento setup:di:compile-multi-tenant' if you have multiple ones.
    7. Clear cache: 'php bin/magento cache:clean', 'php bin/magento cache:flush'