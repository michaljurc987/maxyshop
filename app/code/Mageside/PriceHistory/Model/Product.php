<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\PriceHistory\Model;

use Magento\Framework\Model\ResourceModel\Db\TransactionManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\ObjectRelationProcessor;
use Magento\Framework\Stdlib\DateTime;
use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

class Product extends \Magento\CatalogImportExport\Model\Import\Product
{

    /**
     * @var \Mageside\PriceHistory\Helper\Config
     */
    private $configHelper;

    /**
     * @var ResourceModel\HistoryFactory
     */
    private $historyResourceFactory;

    /**
     * @var array
     */
    private $attributesArray = [];

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    private $localeFormat;

    /**
     * Product constructor.
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\ImportExport\Helper\Data $importExportData
     * @param \Magento\ImportExport\Model\ResourceModel\Import\Data $importData
     * @param \Magento\Eav\Model\Config $config
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\CatalogInventory\Model\Spi\StockStateProviderInterface $stockStateProvider
     * @param \Magento\Catalog\Helper\Data $catalogData
     * @param Import\Config $importConfig
     * @param \Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory $resourceFactory
     * @param \Magento\CatalogImportExport\Model\Import\Product\OptionFactory $optionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setColFactory
     * @param \Magento\CatalogImportExport\Model\Import\Product\Type\Factory $productTypeFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\LinkFactory $linkFactory
     * @param \Magento\CatalogImportExport\Model\Import\Proxy\ProductFactory $proxyProdFactory
     * @param \Magento\CatalogImportExport\Model\Import\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory $stockResItemFac
     * @param DateTime\TimezoneInterface $localeDate
     * @param DateTime $dateTime
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry
     * @param \Magento\CatalogImportExport\Model\Import\Product\StoreResolver $storeResolver
     * @param \Magento\CatalogImportExport\Model\Import\Product\SkuProcessor $skuProcessor
     * @param \Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor $categoryProcessor
     * @param \Magento\CatalogImportExport\Model\Import\Product\Validator $validator
     * @param ObjectRelationProcessor $objectRelationProcessor
     * @param TransactionManagerInterface $transactionManager
     * @param \Magento\CatalogImportExport\Model\Import\Product\TaxClassProcessor $taxClassProcessor
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\Product\Url $productUrl
     * @param \Mageside\PriceHistory\Helper\Config $configHelper
     * @param ResourceModel\HistoryFactory $historyResourceFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param array $data
     * @param array $dateAttrCodes
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Eav\Model\Config $config,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\CatalogInventory\Model\Spi\StockStateProviderInterface $stockStateProvider,
        \Magento\Catalog\Helper\Data $catalogData,
        \Magento\ImportExport\Model\Import\Config $importConfig,
        \Magento\CatalogImportExport\Model\Import\Proxy\Product\ResourceModelFactory $resourceFactory,
        \Magento\CatalogImportExport\Model\Import\Product\OptionFactory $optionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setColFactory,
        \Magento\CatalogImportExport\Model\Import\Product\Type\Factory $productTypeFactory,
        \Magento\Catalog\Model\ResourceModel\Product\LinkFactory $linkFactory,
        \Magento\CatalogImportExport\Model\Import\Proxy\ProductFactory $proxyProdFactory,
        \Magento\CatalogImportExport\Model\Import\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory $stockResItemFac,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        DateTime $dateTime,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\CatalogImportExport\Model\Import\Product\StoreResolver $storeResolver,
        \Magento\CatalogImportExport\Model\Import\Product\SkuProcessor $skuProcessor,
        \Magento\CatalogImportExport\Model\Import\Product\CategoryProcessor $categoryProcessor,
        \Magento\CatalogImportExport\Model\Import\Product\Validator $validator,
        ObjectRelationProcessor $objectRelationProcessor,
        TransactionManagerInterface $transactionManager,
        \Magento\CatalogImportExport\Model\Import\Product\TaxClassProcessor $taxClassProcessor,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product\Url $productUrl,
        \Mageside\PriceHistory\Helper\Config $configHelper,
        \Mageside\PriceHistory\Model\ResourceModel\HistoryFactory $historyResourceFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        array $data = [],
        array $dateAttrCodes = []
    ) {
        $this->configHelper = $configHelper;
        $this->historyResourceFactory = $historyResourceFactory;
        $this->eavConfig = $eavConfig;
        $this->localeFormat = $localeFormat;

        parent::__construct(
            $jsonHelper,
            $importExportData,
            $importData,
            $config,
            $resource,
            $resourceHelper,
            $string,
            $errorAggregator,
            $eventManager,
            $stockRegistry,
            $stockConfiguration,
            $stockStateProvider,
            $catalogData,
            $importConfig,
            $resourceFactory,
            $optionFactory,
            $setColFactory,
            $productTypeFactory,
            $linkFactory,
            $proxyProdFactory,
            $uploaderFactory,
            $filesystem,
            $stockResItemFac,
            $localeDate,
            $dateTime,
            $logger,
            $indexerRegistry,
            $storeResolver,
            $skuProcessor,
            $categoryProcessor,
            $validator,
            $objectRelationProcessor,
            $transactionManager,
            $taxClassProcessor,
            $scopeConfig,
            $productUrl,
            $data,
            $dateAttrCodes
        );
    }

    /**
     * @param array $attributesData
     * @return $this
     */
    protected function _saveProductAttributes(array $attributesData)
    {
        $oldPrices = [];
        if ($this->configHelper->isEnabled()) {
            $oldPrices = $this->getProductPrices($attributesData);
        }

        parent::_saveProductAttributes($attributesData);

        if ($this->configHelper->isEnabled()) {
            $newPrices = $this->getProductPrices($attributesData);
            $this->saveDifferences($oldPrices, $newPrices);
        }

        return $this;
    }

    /**
     * @param $attributesData
     * @return array
     */
    public function getProductPrices($attributesData)
    {
        $attributes = explode(',', $this->configHelper->getConfigModule('prices'));
        $this->attributesArray = $attributes;
        $data = [];
        $resourceTable = $this->getResource()->getTable('catalog_product_entity_decimal');
        if (isset($attributesData[$resourceTable])) {
            $skus = array_keys($attributesData[$resourceTable]);
            $selectOriginal = $this->_connection->select()
                ->from(['e' => $this->getResource()->getTable('catalog_product_entity')], ['entity_id'])
                ->where('e.sku in (?)', $skus);
            foreach ($attributes as $attributeCode) {
                $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);

                $selectOriginal->joinLeft(
                    [$attributeCode.'_table' => $resourceTable],
                    "e.entity_id = {$attributeCode}_table.entity_id and {$attributeCode}_table.store_id = 0"
                    . " and {$attributeCode}_table.attribute_id = {$attribute->getId()}",
                    [$attributeCode => "{$attributeCode}_table.value"]
                );
            }
            $data = $this->_connection->fetchAll($selectOriginal);
        }

        return $data;
    }

    /**
     * @param $data
     * @return array
     */
    public function groupProductData($data)
    {
        $newArray = [];
        foreach ($data as $item) {
            $itemData = [];
            foreach ($this->attributesArray as $attributeCode) {
                if (isset($item[$attributeCode])) {
                    $itemData[$attributeCode] = $item[$attributeCode];
                }
            }
            $newArray[$item['entity_id']] = $itemData;
        }

        return $newArray;
    }

    /**
     * @param $oldPrices
     * @param $newPrices
     */
    public function saveDifferences($oldPrices, $newPrices)
    {
        $oldPrices = $this->groupProductData($oldPrices);
        $newPrices = $this->groupProductData($newPrices);
        $dataToSave = [];
        foreach ($newPrices as $productId => $productData) {
            foreach ($this->attributesArray as $attributeCode) {
                $oldPrice = isset($oldPrices[$productId][$attributeCode])
                    ? $oldPrices[$productId][$attributeCode] : 0.00;

                if (isset($productData[$attributeCode])
                    && $productData[$attributeCode] != $oldPrice
                ) {
                    $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
                    $dataToSave[] = [
                        'product_id'        => $productId,
                        'attribute_id'      => $attribute->getId(),
                        'manager_id'        => $this->configHelper->getManagerId(),
                        'old_price'         => $this->localeFormat->getNumber($oldPrice),
                        'new_price'         => $this->localeFormat->getNumber($productData[$attributeCode])
                    ];
                }
            }
        }

        if (!empty($dataToSave)) {
            $this->historyResourceFactory->create()->bulkPriceChangeInsert($dataToSave);
        }
    }
}
