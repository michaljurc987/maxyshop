<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\PriceHistory\Model\Plugin;

class ProductPrice
{
    /**
     * @var \Mageside\PriceHistory\Helper\Config
     */
    private $configHelper;

    /**
     * @var
     */
    private $historyModelFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    private $localeFormat;

    /**
     * ProductPrice constructor.
     * @param \Mageside\PriceHistory\Helper\Config $configHelper
     * @param \Mageside\PriceHistory\Model\HistoryFactory $historyModelFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     */
    public function __construct(
        \Mageside\PriceHistory\Helper\Config $configHelper,
        \Mageside\PriceHistory\Model\HistoryFactory $historyModelFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat
    ) {
        $this->configHelper = $configHelper;
        $this->historyModelFactory = $historyModelFactory;
        $this->eavConfig = $eavConfig;
        $this->localeFormat = $localeFormat;
    }

    /**
     * @param \Magento\Catalog\Model\Product $subject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeBeforeSave(\Magento\Catalog\Model\Product $subject)
    {
        if ($this->configHelper->isEnabled()) {
            $attributes = explode(',', $this->configHelper->getConfigModule('prices'));
            foreach ($attributes as $attributeCode) {
                $origPrice = $subject->getOrigData($attributeCode);
                $price = $subject->getData($attributeCode);
                $productId = $subject->getData('entity_id');
                if ($origPrice != $price && isset($productId) && !$subject->isObjectNew()) {
                    $attribute = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
                    $data = [
                        'attribute_id'      => $attribute->getId(),
                        'product_id'        => $productId,
                        'manager_id'        => $this->configHelper->getManagerId(),
                        'old_price'         => $this->localeFormat->getNumber($origPrice),
                        'new_price'         => $this->localeFormat->getNumber($price)
                    ];

                    $this->historyModelFactory->create()
                        ->addData($data)
                        ->save();
                }
            }
        }
    }
}
