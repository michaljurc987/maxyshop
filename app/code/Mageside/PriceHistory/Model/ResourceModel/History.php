<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\PriceHistory\Model\ResourceModel;

class History extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('history_price', 'id');
    }

    /**
     * @param $data
     * @return $this
     */
    public function bulkPriceChangeInsert($data)
    {
        $this->getConnection()->insertMultiple($this->getMainTable(), $data);

        return $this;
    }
}
