<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\PriceHistory\Helper;

use Magento\Authorization\Model\UserContextInterface;

/**
 * Class Config
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $_authSession;

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * Config constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param UserContextInterface $userContext
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        UserContextInterface $userContext
    ) {
        $this->_authSession = $authSession;
        $this->userContext = $userContext;

        parent::__construct($context);
    }

    /**
     * Get module settings
     *
     * @param $key
     * @return mixed
     */
    public function getConfigModule($key)
    {
        return $this->scopeConfig
            ->getValue(
                'mageside_pricehistory/general/' . $key,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        if ($this->getConfigModule('enabled') && $this->isModuleOutputEnabled('Mageside_PriceHistory')) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed|null
     */
    public function getManagerId()
    {
        $user = $this->_authSession->getUser();
        if ($user && $user->getId()) {
            return $user->getId();
        }

        if ($userId = $this->userContext->getUserId()) {
            return $userId;
        }

        return null;
    }
}
