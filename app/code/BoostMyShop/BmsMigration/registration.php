<?php
    use \Magento\Framework\Component\ComponentRegistrar;

    ComponentRegistrar::register(ComponentRegistrar::MODULE, 'BoostMyShop_BmsMigration', __DIR__);