<?php

namespace BoostMyShop\BmsMigration\Model;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\App\Filesystem\DirectoryList;


class Misc
{
    protected $_objectManagerFactory;
    protected $_objectManager;

    /**
     * Constructor
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        ObjectManagerFactory $objectManagerFactory
    )
    {
        $this->_objectManagerFactory = $objectManagerFactory;

    }

    public function updatePoTotals()
    {
        //Update po products totals
        $poProductCollection = $this->getObjectManager()->create('\BoostMyShop\Supplier\Model\ResourceModel\Order\Product\CollectionFactory')->create();
        
        $poProductCollectionSize = $poProductCollection->getSize();
        $i = 0;
        foreach($poProductCollection as $poProduct)
        {
            $poProduct->save();
            $i ++;
            echo "Purchase order product(s) processed : $i / $poProductCollectionSize \r";
        }

        echo "\n";
        
        //Update PO totals
        $poCollection = $this->getObjectManager()->create('\BoostMyShop\Supplier\Model\ResourceModel\Order\CollectionFactory')->create();
        
        $poCollectionSize = $poCollection->getSize();
        $i = 0;
        foreach($poCollection as $po)
        {
            $po->updateTotals();
            $i ++;
            echo "Purchase order(s) processed : $i / $poCollectionSize \r";
        }
        
        echo "\n";
    }

    protected function getObjectManager()
    {
        if (null == $this->_objectManager) {
            $area = FrontNameResolver::AREA_CODE;
            $this->_objectManager = $this->_objectManagerFactory->create($_SERVER);
            /** @var \Magento\Framework\App\State $appState */
            $appState = $this->_objectManager->get('Magento\Framework\App\State');
            $appState->setAreaCode($area);
            $configLoader = $this->_objectManager->get('Magento\Framework\ObjectManager\ConfigLoaderInterface');
            $this->_objectManager->configure($configLoader->load($area));
        }
        return $this->_objectManager;
    }

}
