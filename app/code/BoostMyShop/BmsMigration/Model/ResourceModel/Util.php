<?php

namespace BoostMyShop\BmsMigration\Model\ResourceModel;


class Util extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('', '');
    }

    public function insert($tableName, $fields)
    {
        $table = $this->getTable($tableName);
        $this->getConnection()->insert($table, $fields);
    }

    public function update($tableName, $fields, $condition)
    {
        $table = $this->getTable($tableName);
        $this->getConnection()->update($table, $fields, $condition);
    }

    public function truncateTable($table)
    {
        $sql = 'delete from '.$table.' where 1;';
        $this->getConnection()->query($sql);
    }

    public function hasTable($tableName)
    {
		try{
			$sql = 'DESC '.$tableName;
		$this->getConnection()->fetchOne($sql);
			return true;
		}catch(\Exception $e){
			return false;
		}
    }
    
    public function hasColumn($tableName, $column)
    {
        $table = $this->getTable($tableName);
        $sql = 'desc '.$table;
        $cols = $this->getConnection()->fetchAll($sql);
        foreach($cols as $col)
        {
            if ($col['Field'] == $column)
                return true;
        }
        return false;
    }

    public function createColumn($tableName, $columnName, $type, $index = false)
    {
        $table = $this->getTable($tableName);
        $sql = 'ALTER TABLE '.$table.' ADD '.$columnName.' '.$type;
        if ($index)
         $sql .= ', ADD INDEX ('.$columnName.')';
        $sql .= ';';
        $this->getConnection()->query($sql);
    }

    public function getColumnValues($tableName, $col)
    {
        $table = $this->getTable($tableName);
        $sql = 'select distinct '.$col.' from '.$table;
        return $this->getConnection()->fetchCol($sql);
    }

    public function getOne($tableName, $keyColumn, $condition)
    {
        $table = $this->getTable($tableName);
        $sql = 'select '.$keyColumn.' from '.$table.' where '.$condition;
        return $this->getConnection()->fetchOne($sql);
    }

}
