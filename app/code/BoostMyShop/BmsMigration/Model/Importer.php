<?php

namespace BoostMyShop\BmsMigration\Model;


class Importer
{
    protected $_config;
    protected $_util;

    protected $_translateCache = [];

    public function __construct(
        \BoostMyShop\BmsMigration\Model\Config $config,
        \BoostMyShop\BmsMigration\Model\ResourceModel\Util $util,
        \Magento\Framework\Module\Manager $moduleManager
    )
    {
        $this->_config = $config;
        $this->_util = $util;
        $this->_moduleManager = $moduleManager;
    }


    public function run($entityType, $directory)
    {

        $result = ['inserted' => 0, 'updated' => 0, 'error' => 0, 'errors' => []];

        $filePath = $directory.$this->_config->getSetting('entities/'.$entityType.'/file_name');
      
        $filesToImportCount = 1;
      
        $tableName = $this->_config->getSetting('entities/'.$entityType.'/table');
        if (!$tableName)
            throw new \Exception('No table configured for '.$entityType);

        $pk = $this->_config->getSetting('entities/'.$entityType.'/pk');
        if (!$pk)
            throw new \Exception('No primary key configured for '.$entityType);

        $uid = $this->_config->getSetting('entities/'.$entityType.'/uid');
        if (!$uid)
            throw new \Exception('No UID configured for '.$entityType);

        $existingUids = $this->_util->getColumnValues($tableName, $uid);

        //Check if stock movements have been exported in multiple files
        if($entityType == 'stock_movement'){

            //If stock_movement_1.xml exists, multiple files to import
            if(is_file($directory.$entityType.'_1.xml')) {

                for($smFilesCount = 1; $smFilesCount < 100; $smFilesCount++){
                    if(!is_file($directory . $entityType . '_' . $smFilesCount . '.xml')){
                        $smFilesCount = $smFilesCount - 1;
                        break;
                    }
                }
                $filesToImportCount = $smFilesCount;
            }
        }

        for ($i = 1; $i <= $filesToImportCount; $i++){

            if($entityType == 'stock_movement' && $filesToImportCount > 1){
                $filePath = $directory.$entityType.'_'.$i.'.xml';
            }

            if (!is_file($filePath))
                throw new \Exception('File '.$filePath.' does not exist');

            echo "Loading XML file... \n";

            if (file_exists($filePath)) {
                $xml = simplexml_load_file($filePath);
            }

            if ((!$xml))
                throw new \Exception('Unable to load xml from '.$filePath);

            $nodeCount = count($xml);

            echo "File loaded, ". $nodeCount . " element(s) found \n";

            $dropShippingEnabled = $this->_moduleManager->isEnabled('BoostMyShop_DropShip');

            $currentNode = 0;
            foreach($xml->children() as $entityNode)
            {
                try
                {
                    $data = [];
                    foreach($entityNode->children() as $fieldNode)
                    {
                        if(!$dropShippingEnabled) {
                            if ($fieldNode->getName() == 'pps_quantity_product'){
                                continue;
                            }
                        }

                        $targetColumn = $this->_config->getSetting('entities/'.$entityType.'/fields/'.$fieldNode->getName().'/to');
                        if ($targetColumn)
                        {
                            $value = $fieldNode->__toString();
                            $value = $this->translateValue($entityType, $fieldNode->getName(), $value);
                            $value = $this->applyMapping($entityType, $fieldNode->getName(), $value);
                            $data[$targetColumn] = $value;
                        }

                        if ($this->_config->getSetting('entities/'.$entityType.'/fields/'.$fieldNode->getName().'/additionnal'))
                        {
                            $targetColumn = $this->_config->getSetting('entities/'.$entityType.'/fields/'.$fieldNode->getName().'/additionnal/to');
                            $value = $fieldNode->__toString();
                            $data[$targetColumn] = $value;
                        }
                    }

                    if (!isset($data[$uid]))
                    {
                        throw new \Exception('Record has no UID value');
                    }

                    if (!in_array($data[$uid], $existingUids)) {
                        $this->_util->insert($tableName, $data);
                        $result['inserted']++;
                    }
                    else
                    {
                        $this->_util->update($tableName, $data, $uid.' = "'.$data[$uid].'"');
                        $result['updated']++;
                    }
                }
                catch(\Exception $ex)
                {
                    $result['error']++;
                    if (!in_array($ex->getMessage(), $result['errors']))
                        $result['errors'][] = $ex->getMessage();
                }

                $currentNode++;
                echo "Element(s) processed : $currentNode / $nodeCount \r";
            }
        }

        echo "\n \n";
        echo "All data have been imported. \n";
        echo "Saving results in JSON file... \n";

        $logPath = $filePath.'.result.json';
        if (file_exists($logPath))
            unlink($logPath);
        file_put_contents($logPath, json_encode($result));

        echo "Results correctly saved \n";
        echo "\n";
      
        return $result;
    }

    public function translateValue($entityType, $column, $value)
    {
        $key = $entityType.'_'.$column.'_'.$value;
        if (isset($this->_translateCache[$key]))
            return $this->_translateCache[$key];

        if ($this->_config->getSetting('entities/'.$entityType.'/fields/'.$column.'/translator/enabled'))
        {
            if ($value) {
                $tableName = $this->_config->getSetting('entities/' . $entityType . '/fields/' . $column . '/translator/table');
                $searchColumn = $this->_config->getSetting('entities/' . $entityType . '/fields/' . $column . '/translator/search');
                $keyColumn = $this->_config->getSetting('entities/' . $entityType . '/fields/' . $column . '/translator/key');

                $value =  str_replace('"','\"',$value);
                $value =  str_replace("'","\'",$value);

                $translatedValue = $this->_util->getOne($tableName, $keyColumn, $searchColumn . '="' . $value . '"');
                if (!$translatedValue)
                    throw new \Exception('Unable to translate value #'.$value." from table ".$tableName);
                $value = $translatedValue;
            }
        }

        $this->_translateCache[$key] = $value;

        return $value;
    }


    public function applyMapping($entityType, $column, $value)
    {
        if ($this->_config->getSetting('entities/'.$entityType.'/fields/'.$column.'/mapping/enabled'))
        {
            if ($value) {
                $mappedValue = $this->_config->getSetting('entities/'.$entityType.'/fields/'.$column.'/mapping/values/'.$value);
                if ($mappedValue)
                    return $mappedValue;
            }
        }
        return $value;
    }
}
