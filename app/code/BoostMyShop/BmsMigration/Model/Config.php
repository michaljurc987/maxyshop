<?php

namespace BoostMyShop\BmsMigration\Model;

class Config
{
    protected $_scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig){
        $this->_scopeConfig = $scopeConfig;
    }

    public function getSetting($path)
    {
        return $this->_scopeConfig->getValue($path, 'store', 0);
    }


}