<?php
namespace BoostMyShop\BmsMigration\Console\Command;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\ObjectManagerFactory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Magento\Framework\Setup\SchemaSetupInterface;


class TruncateData extends Command
{
    protected $_util;
    protected $_state;
    protected $_moduleManager;

    /**
     * Constructor
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \BoostMyShop\BmsMigration\Model\ResourceModel\Util $util,
        \Magento\Framework\Module\Manager $moduleManager    
    )
    {
        $this->_util = $util;
        $this->_state = $state;
        $this->_moduleManager = $moduleManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('bms_migration:truncate_data')->setDescription('Truncate data for specified group')->setDefinition($this->getInputList());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('START');

        $this->_state->setAreaCode('adminhtml');

        $group = $input->getArgument('group');
        switch($group)
        {
            case 'supplier':
                if($this->_moduleManager->isEnabled('BoostMyShop_DropShip')){
                    $this->truncateTables(['bms_supplier_stock_import_details', 'bms_supplier_stock_import', 'bms_supplier_product', 'bms_supplier'], $output);  
                 }else{
                    $this->truncateTables(['bms_supplier_product', 'bms_supplier'], $output);
                 }
                 break;
            case 'po':
                $this->truncateTables(['bms_purchase_order_reception_item', 'bms_purchase_order_reception', 'bms_purchase_order_product', '	bms_purchase_order'], $output);
                break;
            case 'stock':
                $this->truncateTables(['bms_advancedstock_routing_store_warehouse', 'bms_advancedstock_sales_history', 'bms_advancedstock_stock_movement', 'bms_advancedstock_warehouse_item', 'bms_advancedstock_warehouse'], $output);
                break;

        }

        $output->writeln('END');
    }

    protected function truncateTables($tables, $output)
    {
        foreach($tables as $table)
        {
            $output->writeln('=> Truncate table '.$table);
            $this->_util->truncateTable($table);
        }
    }

    public function getInputList()
    {
        return [
            new InputArgument(
                'group',
                InputArgument::REQUIRED,
                'Group to truncate datas among : supplier, po'
            ),
        ];
    }

}
