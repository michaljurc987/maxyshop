<?php
namespace BoostMyShop\BmsMigration\Console\Command;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\ObjectManagerFactory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Magento\Framework\Setup\SchemaSetupInterface;


class Import extends Command
{
    protected $_state;
    protected $_importer;

    /**
     * Constructor
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \BoostMyShop\BmsMigration\Model\Importer $importer
    )
    {
        $this->_state = $state;
        $this->_importer = $importer;

        parent::__construct();
    }

    protected function configure()
    {
        $options = [
            new InputArgument(
                'entity',
                InputArgument::REQUIRED,
                'Entity to import ()'
            ),
            new InputArgument(
                'directory',
                InputArgument::REQUIRED,
                'Directory for input xml files'
            )

        ];

        $this->setName('bms_migration:import')->setDescription('Import data from M1')->setDefinition($options);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_state->setAreaCode('adminhtml');

        $output->writeln('START');

        $entity = $input->getArgument('entity');
        $directory = $input->getArgument('directory');

        if (!$entity)
            throw new \Exception('Entity option is missing');
        if (!$directory)
            throw new \Exception('Directory option is missing');

        $result = $this->_importer->run($entity, $directory);

        $output->writeln('Import complete : ');
        $output->writeln('=> Inserted : '.$result['inserted']);
        $output->writeln('=> Updated : '.$result['updated']);
        $output->writeln('=> Errors : '.$result['error']);

        $output->writeln('END');
    }

    protected function getEntities()
    {
        return ['supplier, purchase_order', 'warehouse', 'inventory'];
    }

}
