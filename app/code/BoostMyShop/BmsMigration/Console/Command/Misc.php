<?php
namespace BoostMyShop\BmsMigration\Console\Command;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\ObjectManagerFactory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Magento\Framework\Setup\SchemaSetupInterface;


class Misc extends Command
{
    protected $_state;
    protected $_misc;

    /**
     * Constructor
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \BoostMyShop\BmsMigration\Model\Misc $misc
    )
    {
        $this->_state = $state;
        $this->_misc = $misc;

        parent::__construct();
    }

    protected function configure()
    {
        $options = [
            new InputArgument(
                'action',
                InputArgument::REQUIRED,
                'Action to proces'
            )
        ];

        $this->setName('bms_migration:misc')->setDescription('Execute additional updates')->setDefinition($options);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_state->setAreaCode('adminhtml');

        $output->writeln('START');

        $action = $input->getArgument('action');
        switch($action)
        {
            case 'update_po_totals':
                $this->_misc->updatePoTotals();
                break;
            default:
                throw new \Exception('Unknown action '.$action);
                break;
        }

        $output->writeln('END');
    }


}
