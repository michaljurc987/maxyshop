<?php
namespace BoostMyShop\BmsMigration\Console\Command;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ObjectManager\ConfigLoader;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\ObjectManagerFactory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Magento\Framework\Setup\SchemaSetupInterface;


class PrepareTables extends Command
{
    protected $_util;
    protected $_state;

    /**
     * Constructor
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \BoostMyShop\BmsMigration\Model\ResourceModel\Util $util
    )
    {
        $this->_util = $util;
        $this->_state = $state;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('bms_migration:prepare_tables')->setDescription('Prepare database tables to import data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('START');

        $this->_state->setAreaCode('adminhtml');

        $tables = [
            'bms_advancedstock_warehouse' => 'w_m1_id',
            'bms_advancedstock_warehouse_item' => 'wi_m1_id',
            'bms_purchase_order' => 'po_m1_id',
            'bms_purchase_order_product' => 'pop_m1_id',
            'bms_supplier' => 'sup_m1_id',
            'bms_supplier_product'  => 'sp_m1_id',
            'bms_advancedstock_stock_movement'  => 'sm_m1_id',
            'bms_rma' => 'rma_m1_id',
            'bms_rma_item' => 'ri_m1_id',
            'bms_rma_history' => 'rh_m1_id'
        ];

        foreach($tables as $table => $column){
			if($this->_util->hasTable($table)){
				$this->insertColumn($table, $column, $output);
			}
		}

        $output->writeln('END');
    }

    protected function insertColumn($table, $columnName, $output)
    {
        if (!$this->_util->hasColumn($table, $columnName))
        {
            $this->_util->createColumn($table, $columnName, 'INT', true);
            $output->writeln('=> Create column '.$columnName.' in table '.$table);
        }
    }

}
