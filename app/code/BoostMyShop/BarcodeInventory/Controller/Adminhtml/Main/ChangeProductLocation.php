<?php
namespace BoostMyShop\BarcodeInventory\Controller\Adminhtml\Main;

use Magento\Framework\Controller\ResultFactory;

class ChangeProductLocation extends \Magento\Backend\App\AbstractAction
{
    protected $barcodeInventoryRegistry;
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \BoostMyShop\BarcodeInventory\Model\Registry $barcodeInventoryRegistry,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);

        $this->_barcodeInventoryRegistry = $barcodeInventoryRegistry;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $productId = $this->getRequest()->getParam('id');
        $newLocation = $this->getRequest()->getParam('location');
        $warehouseId =  $this->_barcodeInventoryRegistry->getCurrentWarehouseId();

        $result = $this->resultJsonFactory->create();

        $data = [];
        try
        {
            $this->_objectManager->get('\BoostMyShop\BarcodeInventory\Model\ProductInformation')->setShelfLocation($productId, $warehouseId, $newLocation);

            $data['success'] = true;
            $data['msg'] = __('Location updated');
        }
        catch(\Exception $ex)
        {

            $data['success'] = false;
            $data['msg'] = $ex->getMessage();
        }

        return $result->setData($data);

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Boostmyshop_Barcodeinventory::Main');
    }

}