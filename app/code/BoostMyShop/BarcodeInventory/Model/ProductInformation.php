<?php

namespace BoostMyShop\BarcodeInventory\Model;

use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;

class ProductInformation
{
    protected $_productRepository;
    protected $_barcodeInventoryRegistry;
    protected $_objectManager;
    protected $_stockState;
    protected $_config;
    protected $_logger;
    protected $_collectionFactory;
    protected $_storeManager;

    public function __construct(
            ProductRepositoryInterface $productRepository,
            ObjectManagerInterface $om,
            \Magento\CatalogInventory\Api\StockStateInterface $stockState,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
            \BoostMyShop\BarcodeInventory\Model\Config\BarcodeInventory $config,
            \BoostMyShop\BarcodeInventory\Helper\Logger $logger,
            \BoostMyShop\BarcodeInventory\Model\Registry $barcodeInventoryRegistry,
            \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_objectManager = $om;
        $this->_productRepository = $productRepository;
        $this->_barcodeInventoryRegistry = $barcodeInventoryRegistry;
        $this->_stockState = $stockState;
        $this->_collectionFactory = $collectionFactory;
        $this->_config = $config;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
    }


    public function getJsonDataForBarcode($barcode)
    {
        $productId = $this->getIdFromBarcode($barcode);
        if (!$productId)
            throw new \Exception(__('No product found with barcode %1', $barcode));

        $product = $this->_productRepository->getById($productId);

        $data['id'] = $product->getId();
        $data['name'] = $product->getName();
        $data['sku'] = $product->getSku();
        $data['image_url'] = $this->getImage($product);
        $data['barcode'] = $barcode;
        $data['product_url'] = $this->getProductUrl($product->getId());

        $warehouseId = $this->_barcodeInventoryRegistry->getCurrentWarehouseId();
        $data['qty'] = $this->getStock($product->getId(), $warehouseId);
        $data['location'] = $this->getShelfLocation($product->getId(), $warehouseId);

        $data['misc']  = $this->getMisc($product, $warehouseId);

        return $data;
    }

    //return supplier information (if exists)
    protected function getMisc($product, $warehouseId)
    {
        $html = '';

        if ($this->_config->isSupplierIsInstalled())
        {
            $primarySupplier = $this->_objectManager->get('\BoostMyShop\Supplier\Model\ResourceModel\Supplier\Product\Collection')
                                        ->getSuppliers($product->getId())
                                        ->getFirstItem();
            if ($primarySupplier->getId())
            {
                $html .= $primarySupplier->getsup_name().' - '.$primarySupplier->getsp_sku();
            }

        }

        return $html;
    }

    protected function getIdFromBarcode($barcode)
    {
        $barcodeAttribute = $this->getBarcodeAttribute();

        //standard search
        $item = $this->_collectionFactory->create()->addAttributeToFilter($barcodeAttribute, $barcode)->getFirstItem();
        if ($item->getId())
            return $item->getId();

        //if not found, try to play with the leading 0
        if ($barcode[0] == "0") //if barcode starts with a 0, try to find result without leading 0
        {
            $newBarcode = substr($barcode, 1);
            $item = $this->_collectionFactory->create()->addAttributeToFilter($barcodeAttribute, $newBarcode)->getFirstItem();
            if ($item->getId())
                return $item->getId();
        }

        //if barcode doesnt start with 0, try to find the result with leading 0
        $newBarcode = "0".$barcode;
        $item = $this->_collectionFactory->create()->addAttributeToFilter($barcodeAttribute, $newBarcode)->getFirstItem();
        if ($item->getId())
            return $item->getId();

        return false;
    }

    protected function getImage($product)
    {
        $helper = $this->_objectManager->get('\Magento\Catalog\Helper\Product');
        return $helper->getSmallImageUrl($product);
    }

    protected function getBarcodeAttribute()
    {
        $config = $this->_objectManager->get('\BoostMyShop\BarcodeInventory\Model\Config\BarcodeInventory');
        return $config->getSetting('general/barcode_attribute');
    }

    /**
     * Return stock level
     *
     * @param $productId
     * @return mixed
     */
    protected function getStock($productId, $warehouseId)
    {
        $value = 0;
        if (!$this->_config->isAdvancedStockIsInstalled())
            $value = $this->_stockState->getStockQty($productId);
        else
        {
            $stockItem = $this->_objectManager->create('\BoostMyShop\AdvancedStock\Model\Warehouse\Item')->loadByProductWarehouse($productId, $warehouseId);
            $currentQty = $stockItem->getwi_physical_quantity();
            if($currentQty && $currentQty > 0)
                $value = $currentQty;
        }
        $this->_logger->log('getStock for product #'.$productId.' and warehouse #'.$warehouseId.' is '.$value.' (advanced stock is '.($this->_config->isAdvancedStockIsInstalled() ? '' : 'NOT').' installed)');
        return $value;
    }

    protected function getShelfLocation($productId, $warehouseId)
    {
        $value = '';
        if (!$this->_config->isAdvancedStockIsInstalled())
            $value = '';
        else
        {
            $stockItem = $this->_objectManager->create('\BoostMyShop\AdvancedStock\Model\Warehouse\Item')->loadByProductWarehouse($productId, $warehouseId);
            $value = $stockItem->getwi_shelf_location();
        }
        return $value;
    }

    public function setShelfLocation($productId, $warehouseId, $newLocation)
    {
        if ($this->_config->isAdvancedStockIsInstalled())
        {
            $stockItem = $this->_objectManager->create('\BoostMyShop\AdvancedStock\Model\Warehouse\Item')->loadByProductWarehouse($productId, $warehouseId);
            $stockItem->setwi_shelf_location($newLocation)->save();
        }
    }

    protected function getProductUrl($productId)
    {
        if (!$this->_config->isAdvancedStockIsInstalled())
        {
            return $this->getUrl('catalog/product/edit', ['id' => $productId]);
        }
        else
        {
            return $this->getUrl('erp/products/edit', ['id' => $productId]);
        }
    }

    protected function getUrl($url, $params = [])
    {
        return $this->_storeManager->getStore(0)->getUrl($url, $params);
    }

}
