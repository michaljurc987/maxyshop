<?php

namespace BoostMyShop\BarcodeLabel\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Controller\Adminhtml\Product\Builder as ProductBuilder;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintLabel extends \Magento\Backend\App\AbstractAction
{
    protected $_label;
    protected $_filesystem;
    protected $_labelHelper;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     */
    public function __construct(
        Context $context,
        ProductBuilder $productBuilder,
        \Magento\Framework\Filesystem $filesystem,
        \BoostMyShop\BarcodeLabel\Helper\Label $labelHelper
    ) {
        $this->productBuilder = $productBuilder;
        $this->_filesystem = $filesystem;
        $this->_labelHelper = $labelHelper;

        parent::__construct($context);
    }

    public function execute()
    {
        $this->_auth->getAuthStorage()->setIsFirstPageAfterLogin(false);

        $count = (int)$this->getRequest()->getParam('qty');
        $product = $this->productBuilder->build($this->getRequest());

        $products[] = ['product' => $product, 'qty' => $count];

        try {
            $label = $this->_labelHelper->getLabelDetails($products);
            $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                $label['filename'],
                $label['content'],
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            //Delete file
            $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
//            $dir->delete($label['filename']);

        }catch(\Exception $e){
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        }

    }

    protected function _isAllowed()
    {
        return true;
    }

}
