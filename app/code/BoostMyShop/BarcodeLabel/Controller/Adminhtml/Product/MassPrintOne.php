<?php

namespace BoostMyShop\BarcodeLabel\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Controller\Adminhtml\Product\Builder as ProductBuilder;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class MassPrintOne extends \Magento\Backend\App\AbstractAction
{
    protected $_label;
    protected $filter;
    protected $collectionFactory;
    protected $productFactory;
    protected $_stockState;
    protected $_filesystem;
    protected $_labelHelper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Filesystem $filesystem,
        \BoostMyShop\BarcodeLabel\Helper\Label $labelHelper
    ) {
        $this->productFactory = $productFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_stockState = $stockState;
        $this->_filesystem = $filesystem;
        $this->_labelHelper = $labelHelper;

        parent::__construct($context);
    }

    public function execute()
    {
        $products = [];

        $productIds = $this->getRequest()->getPost('massaction');

        $collection = $this->collectionFactory->create()->addFieldToFilter('entity_id', ['in' => $productIds]);
        foreach ($collection->getItems() as $product) {

            $product = $this->productFactory->create()->load($product->getId());
            $products[] = ['product' => $product, 'qty' => 1];
        }

        try {
            $label = $this->_labelHelper->getLabelDetails($products);

            $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                $label['filename'],
                $label['content'],
                DirectoryList::VAR_DIR,
                'application/pdf'
            );

            //Delete file
            $dir = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $dir->delete($label['filename']);

        }catch(\Exception $e){
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_redirect('*/*/index');
        }
    }

    protected function _isAllowed()
    {
        return true;
    }

}
