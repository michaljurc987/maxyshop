<?php

namespace BoostMyShop\BarcodeLabel\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class ErpProductViewButtons implements ObserverInterface
{
    public function execute(EventObserver $observer)
    {
        $block = $observer->getEvent()->getblock();
        $product = $observer->getEvent()->getproduct();

        if($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
        {
            $url = $block->getUrl('barcodelabel/product/printLabel', ['qty' => 'param_qty', 'id' => $product->getId()]);

            $block->addButton(
                'print_barcode_label',
                [
                    'id' => 'print_barcode_label',
                    'label' => __('Barcode label'),
                    'onclick' => 'printBarcodeLabel(\''.__('How many label do you want to print ?').'\', \''.$url.'\', 1)'
                ],
                -1
            );
        }


    }


}