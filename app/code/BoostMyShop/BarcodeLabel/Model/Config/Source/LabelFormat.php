<?php

namespace BoostMyShop\BarcodeLabel\Model\Config\Source;

class LabelFormat implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        $options = array();

        $options[] = array('value' => 'pdf', 'label' => 'PDF');
        $options[] = array('value' => 'zpl', 'label' => 'ZPL');

        return $options;
    }

}
