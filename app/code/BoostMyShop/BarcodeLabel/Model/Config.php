<?php

namespace BoostMyShop\BarcodeLabel\Model;

class Config
{
    protected $_scopeConfig;
    protected $_currency;
    protected $_moduleManager;
    protected $request;


    /**
     * Config constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleManager = $moduleManager;
        $this->_currency = $currency;
        $this->request = $request;
    }

    public function getSetting($path, $storeId = 0)
    {
        $websiteId =$this->request->getParam('website');
        if($websiteId)
            return $this->_scopeConfig->getValue('barcodelabel/'.$path, 'website', $websiteId);

        return $this->_scopeConfig->getValue('barcodelabel/'.$path, 'store', $storeId);
    }

    public function isEnabled()
    {
        return $this->getSetting('general/enable');
    }

    /**
     * @return bool
     */
    public function isAdvancedStockEnabled()
    {
        return  $this->_moduleManager->isEnabled('BoostMyShop_AdvancedStock');
    }

    public function getBarcodeAttribute()
    {
        $value = $this->getSetting('general/barcode_attribute');
        if (!$value) {
            throw new \Exception('Barcode attribute is not configured in Stores > Configuration > BoostMyShop > Barcode Label');
        }
        return $value;
    }

    public function getManufacturerAttribute()
    {
        $value = $this->getSetting('attributes/manufacturer');
        return $value;
    }

    /*
     * return int warehouse id
     */
    public function getWarehouseForLocations()
    {
        return $this->getSetting("attributes/warehouse_for_locations");
    }

    public function getCurrencySymbol()
    {
        $currencyCode = $this->_scopeConfig->getValue('currency/options/base', 'store', 0);
        $currency = $this->_currency->load($currencyCode);
        return $currency->getCurrencySymbol();
    }

    public function isSupplierEnabled()
    {
        return  $this->_moduleManager->isEnabled('BoostMyShop_Supplier');
    }
}