<?php

namespace BoostMyShop\BarcodeLabel\Block\Order\Edit\Tab\Reception\Renderer;

use Magento\Framework\DataObject;

class PrintBarcode extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $row)
    {

        $url = $this->getUrl('barcodelabel/purchaseOrder/printReception', ['id' => $row->getId(),"po_id"=> $this->getRequest()->getParam("po_id")]);
        $html = '<a id = "barcode_print" href="'.$url.'">'.__('Print').'</a>';
        return $html;
    }
}