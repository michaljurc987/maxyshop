<?php

namespace BoostMyShop\BarcodeLabel\Block\Rewrite\Order\Edit\Tab\Products\Renderer;

use Magento\Framework\DataObject;

class Barcode extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_config = null;
    protected $_objectManager;

    public function __construct(
        \BoostMyShop\BarcodeLabel\Model\Config $config,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_config = $config;
        $this->_objectManager = $objectManager;
    }

    public function render(DataObject $row)
    {
        $html = '';
        $html .= '<a data-qty="'.$row->getPendingQty().'" data-product-id = "'.(int)$row->getpop_product_id().'" class="bacode-poitem" href="javascript:void(0)">'.$row->getProduct()->getData($this->getColumn()->getIndex()).'</a><br>';
        if($this->_config->isSupplierEnabled()){
            $mpn = $this->_objectManager->get('\BoostMyShop\Supplier\Model\Config')->getMpnAttribute();
            if($mpn && $row->getProduct()->getData($mpn))
                $html .= '<span>MPN: '.$row->getProduct()->getData($mpn).'</span>';
        }

        return $html;
    }

}
