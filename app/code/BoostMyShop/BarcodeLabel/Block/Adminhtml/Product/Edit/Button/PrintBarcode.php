<?php
namespace BoostMyShop\BarcodeLabel\Block\Adminhtml\Product\Edit\Button;

use Magento\Catalog\Block\Adminhtml\Product\Edit\Button\Generic;

class PrintBarcode extends Generic
{
 

    public function getButtonData()
    {
        $url = $this->getUrl('barcodelabel/product/printLabel', ['qty' => 'param_qty', 'id' => $this->getProduct()->getId()]);
        $defaultQty = 1;

        return [
            'label' => __('Barcode Label'),
            'on_click' => "printBarcodeLabel('".__('How many label do you want to print ?')."', '".$url."', ".$defaultQty.")",
            'sort_order' => 100
        ];
    }

    public function getProduct()
    {
        return $this->registry->registry('current_product');
    }

}
