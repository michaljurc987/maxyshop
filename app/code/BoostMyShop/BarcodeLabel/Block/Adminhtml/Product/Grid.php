<?php

namespace BoostMyShop\BarcodeLabel\Block\Adminhtml\Product;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_productCollectionFactory;
    protected $_config;
    protected $_eavConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \BoostMyShop\BarcodeLabel\Model\Config $config,
        array $data = []
    ) {

        parent::__construct($context, $backendHelper, $data);

        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_config = $config;
        $this->_eavConfig = $eavConfig;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('barcodeLabelGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Barcode Label'));

        $this->setSaveParametersInSession(true);
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*');
        $this->setCollection($collection);

        $collection->joinField(
            'qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left'
        );

        return parent::_prepareCollection();
    }


    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('image', ['header' => __('Image'),'filter' => false, 'sortable' => false, 'type' => 'renderer', 'renderer' => '\BoostMyShop\BarcodeLabel\Block\Adminhtml\Widget\Renderer\Image']);
        $this->addColumn('entity_id', ['header' => __('#'), 'index' => 'entity_id', 'type' => 'number']);
        $this->addColumn('sku', ['header' => __('Sku'), 'index' => 'sku', 'type' => 'renderer', 'renderer' => '\BoostMyShop\BarcodeLabel\Block\Adminhtml\Widget\Renderer\Sku']);
        $this->addColumn('name', ['header' => __('Product'), 'index' => 'name']);
        $this->addColumn('barcode', ['header' => __('Barcode'), 'index' => $this->_config->getBarcodeAttribute()]);

        if ($this->_config->getManufacturerAttribute())
        {
            $this->addColumn('manufacturer', ['header' => __('Manufacturer'), 'index' => $this->_config->getManufacturerAttribute(), 'type' => 'options', 'options' => $this->getManufacturerOptions()]);
        }

        $this->addColumn('stock', ['header' => __('Stock'), 'index' => 'qty', 'type' => 'number']);
        $this->addColumn('print', ['header' => __('Print'),'filter' => false, 'sortable' => false, 'align' => 'center', 'type' => 'renderer', 'renderer' => '\BoostMyShop\BarcodeLabel\Block\Adminhtml\Widget\Renderer\PrintLabel']);

        return parent::_prepareColumns();
    }

    public function getRowUrl($item){
        //empty to not get link to #
    }

    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('entity_id');

        $this->getMassactionBlock()->addItem(
            'print_one',
            [
                'label' => __('Print one label per product'),
                'url' => $this->getUrl('*/*/massPrintOne'),
                'confirm' => __('Are you sure?')
            ]
        );

        $this->getMassactionBlock()->addItem(
            'print_qty',
                [
                    'label' => __('Print label from quantities'),
                    'url' => $this->getUrl('*/*/massPrintQty'),
                    'confirm' => __('Are you sure?')
                ]
        );

        return $this;
    }

    public function getManufacturerOptions()
    {
        $options = array();

        $attribute = $this->_eavConfig->getAttribute('catalog_product', $this->_config->getManufacturerAttribute());
        $attributeOptions = $attribute->getSource()->getAllOptions();

        foreach($attributeOptions as $item)
        {
            $options[$item['value']] = $item['label'];
        }

        return $options;
    }

}
