<?php

namespace BoostMyShop\BarcodeLabel\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

class Label
{
    protected $_configFactory;
    protected $_objectManager;

    public function __construct(
        \BoostMyShop\BarcodeLabel\Model\ConfigFactory $config,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ){
        $this->_configFactory = $config;
        $this->_objectManager = $objectManager;
    }

    public function getLabelDetails($products)
    {
        $result = ['content' => '', 'filename' => ''];
        if ($this->getLabelFormat()  == 'zpl'){
            $result['content'] = $this->_objectManager->create('BoostMyShop\BarcodeLabel\Model\Zpl')->getZpl($products);
            $result['filename'] = 'barcode_label.zpl';
        } else{
            $result['content'] = $this->_objectManager->create('BoostMyShop\BarcodeLabel\Model\Pdf')->getPdf($products)->render();
            $result['filename'] = 'barcode_label.pdf';
        }

        return $result;
    }

    protected function getLabelFormat()
    {
        return $this->_configFactory->create()->getSetting('general/label_format');
    }

}
