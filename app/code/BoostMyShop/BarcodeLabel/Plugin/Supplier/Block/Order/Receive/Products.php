<?php

namespace BoostMyShop\BarcodeLabel\Plugin\Supplier\Block\Order\Receive;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\ObjectManager;

class Products
{
    protected $_template = 'BoostMyShop_Supplier::Order/Receive/Products.phtml';

    public function aroundGetProductBarcode(\BoostMyShop\Supplier\Block\Order\Receive\Products $subject, $proceed, $item)
    {
        $html = '';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('\BoostMyShop\Supplier\Model\Product');
        $html .= '<a href="#" data-product-id="'.(int)$item->getpop_product_id().'" data-qty="'.(int)$item->getPendingQty().'" class="openBarcodePopup">'.$product->getBarcode($item->getpop_product_id()).'</a><br>';
        if($product->getMpn($item->getpop_product_id())){
            $html .= '<span>MPN: '.$product->getMpn($item->getpop_product_id()).'</a>';
        }
        return $html;
    }

}