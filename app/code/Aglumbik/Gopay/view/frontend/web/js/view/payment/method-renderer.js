define(
        [
            'uiComponent',
            'Magento_Checkout/js/model/payment/renderer-list'
        ],
        function (
                Component,
                rendererList
                ) {
            'use strict';
            rendererList.push(
                    {
                        type: 'gopay_bitcoin',
                        component: 'Aglumbik_Gopay/js/view/payment/method-renderer/gopay_bitcoin'
                    }
            );
            rendererList.push(
                    {
                        type: 'gopay_cc',
                        component: 'Aglumbik_Gopay/js/view/payment/method-renderer/gopay_cc'
                    }
            );
            rendererList.push(
                    {
                        type: 'gopay_googlepay',
                        component: 'Aglumbik_Gopay/js/view/payment/method-renderer/gopay_googlepay'
                    }
            );
            return Component.extend({});
        }
);
