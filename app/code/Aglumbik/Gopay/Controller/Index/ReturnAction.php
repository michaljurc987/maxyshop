<?php

namespace Aglumbik\Gopay\Controller\Index;

use Magento\Framework\Registry;

class ReturnAction extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $checkoutSession;
    protected $responseFactory;
    protected $scopeConfig;
    protected $registry;
    protected $order;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Checkout\Model\Session $checkoutSession, Registry $registry, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Sales\Api\Data\OrderInterface $order, \Magento\Framework\App\ResponseFactory $responseFactory) {
        $this->resultPageFactory = $resultPageFactory;
        $this->checkoutSession = $checkoutSession;
        $this->responseFactory = $responseFactory;
        $this->scopeConfig = $scopeConfig;
        $this->registry = $registry;
        $this->order = $order;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id != 0) {

            // Get config params
            $clientId = $this->scopeConfig->getValue('payment/gopay/client_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $clientSecret = $this->scopeConfig->getValue('payment/gopay/client_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $gatewayUrl = $this->scopeConfig->getValue('payment/gopay/gateway_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            // Get Access Token
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $gatewayUrl . "oauth2/token");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "Content-Type: application/x-www-form-urlencoded"));
            curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $clientSecret);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=payment-create");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $json = json_decode($result, true);
            $accessToken = $json['access_token'];
            curl_close($ch);

            // Get Payment Info
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $gatewayUrl . "payments/payment/" . $id);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "application/x-www-form-urlencoded", "Authorization: Bearer " . $accessToken));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            $json = json_decode($result, true);
            $paymentStatus = $json['state'];

            if ($paymentStatus == 'PAID') {

                // Payment complete
                $incrementId = $json['order_number'];
                $order = $this->order->loadByIncrementId($incrementId);
                $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                $order->addStatusToHistory($order->getStatus(), 'GoPay - Payment Successful');
                $order->save();
                $this->responseFactory->create()->setRedirect($this->_url->getUrl('checkout/onepage/success'))->sendResponse();
            } else if ($paymentStatus == 'PAYMENT_METHOD_CHOSEN') {

                // Waiting for payment
                $incrementId = $json['order_number'];
                $order = $this->order->loadByIncrementId($incrementId);
                $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
                $order->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
                $order->addStatusToHistory($order->getStatus(), 'GoPay - Waiting for payment');
                $order->save();
                $this->responseFactory->create()->setRedirect($this->_url->getUrl('checkout/onepage/success'))->sendResponse();
            } else {

                // Cancel order
                $incrementId = $json['order_number'];
                $order = $this->order->loadByIncrementId($incrementId);
                $order->addStatusToHistory($order->getStatus(), 'GoPay - Order Canceled: ' . $paymentStatus);
                $order->save();
                $order->cancel();
                $this->responseFactory->create()->setRedirect($this->_url->getUrl('/'))->sendResponse();
            }
        } else {
            // Redirect to Homepage
            $this->responseFactory->create()->setRedirect($this->_url->getUrl('/'))->sendResponse();
        }
        
    }

}
