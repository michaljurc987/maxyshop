<?php

namespace Aglumbik\Gopay\Controller\Index;

use Magento\Framework\Registry;

class Redirect extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $checkoutSession;
    protected $registry;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Checkout\Model\Session $checkoutSession, Registry $registry) {
        $this->resultPageFactory = $resultPageFactory;
        $this->checkoutSession = $checkoutSession;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $order = $this->checkoutSession->getLastRealOrder();
        $orderId = $order->getEntityId();

        // Set order ID into registry
        $this->registry->register('order_id', $orderId);
		
        // Return
        return $this->resultPageFactory->create();
    }

}
