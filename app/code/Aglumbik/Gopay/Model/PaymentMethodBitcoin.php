<?php
 
namespace Aglumbik\Gopay\Model;
 
/**
 * Pay In Store payment method model
 */
class PaymentMethodBitcoin extends \Magento\Payment\Model\Method\AbstractMethod
{
 
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'gopay_bitcoin';
}