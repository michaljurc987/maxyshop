<?php

namespace Aglumbik\Gopay\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Language implements ArrayInterface {

    public function toOptionArray() {
        return [
                ['value' => 'cs', 'label' => __('Čeština')],
                ['value' => 'en', 'label' => __('Angličtina')],
                ['value' => 'sk', 'label' => __('Slovenčina')],
                ['value' => 'de', 'label' => __('Nemčina')],
                ['value' => 'ru', 'label' => __('Ruština')],
                ['value' => 'fr', 'label' => __('Francúzština')],
                ['value' => 'pl', 'label' => __('Poľština')],
                ['value' => 'hu', 'label' => __('Maďarčina')],
                ['value' => 'ro', 'label' => __('Rumunčina')],
                ['value' => 'bg', 'label' => __('Bulharčina')],
                ['value' => 'hr', 'label' => __('Chorvátština')],
                ['value' => 'it', 'label' => __('Taliančina')],
                ['value' => 'es', 'label' => __('Španielčina')],
        ];
    }

}
