<?php

namespace Aglumbik\Gopay\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;

class Redirect extends Template {

    protected $_request;
    protected $_registry;
    protected $_storeManager;
    protected $_order;

    public function __construct(Template\Context $context, Registry $registry, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\OrderFactory $salesOrderFactory, Order $order, array $data = []) {
        $this->_registry = $registry;
        $this->_order = $order;
        parent::__construct($context, $data);
    }

    public function getGatewayUrl() {
        // Load order
        $orderId = $this->_registry->registry('order_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order');
        $order->load($orderId);
		
        /* Items */
        $orderItems = array();

        /* Shipment */
        if ((float) $order->getShippingInclTax() != 0) {
            $orderItems[] = array("count" => 1, "name" => substr($order->getShippingDescription(), 0, 256), "amount" => (($order->getShippingInclTax() / 1) * 100));
        }
		
		/* Discount */
        if ((float) $order->getDiscountAmount() != 0) {
            $orderItems[] = array("count" => 1, "name" => 'Sleva', "amount" => ((($order->getDiscountAmount() / 1) * 100)));
		}
        
        /* Items */
        $items = $order->getAllItems();
        foreach ($items as $item) {
			if((float) $item->getOriginalPrice() > 0) {
				$qty = (int) $item->getQtyOrdered();
				$amount = (($item->getOriginalPrice() / 1) * 100);
				$orderItems[] = array("count" => $qty, "name" => substr($item->getName(), 0, 256), "amount" => ($qty * $amount));
			}
		}

        // Get config params
        $goId = $this->_scopeConfig->getValue('payment/gopay/go_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $secureKey = $this->_scopeConfig->getValue('payment/gopay/secure_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $clientId = $this->_scopeConfig->getValue('payment/gopay/client_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $clientSecret = $this->_scopeConfig->getValue('payment/gopay/client_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $language = $this->_scopeConfig->getValue('payment/gopay/gateway_language', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $gatewayUrl = $this->_scopeConfig->getValue('payment/gopay/gateway_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        // Access token
        $accessToken = '';

        // Get Access Token
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $gatewayUrl . 'oauth2/token');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_USERPWD, $clientId . ":" . $clientSecret);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=payment-create");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $json = json_decode($result, true);
        $accessToken = $json['access_token'];
        curl_close($ch);
		
		
		// Get selected payment method 
		$method = $order->getPayment()->getMethod();
		$paymentInstrument = 'BITCOIN';
		if($method == 'gopay_cc') {
			$paymentInstrument = 'PAYMENT_CARD';
		} else if($method == 'gopay_googlepay') {
			$paymentInstrument = 'GPAY';
		}

        // Create Payment
        $data = array();
        $data['payer'] = array('default_payment_instrument' => $paymentInstrument, 'allowed_payment_instruments' => array($paymentInstrument), 'contact' => array('first_name' => $order->getBillingAddress()->getFirstname(), 'last_name' => $order->getBillingAddress()->getLastname(), 'email' => $order->getBillingAddress()->getEmail()));
        $data['target'] = array('type' => 'ACCOUNT', 'goid' => $goId);
        $data['amount'] = (int) ($order->getGrandTotal() * 100);
        $data['currency'] = $order->getOrderCurrencyCode();
        $data['order_number'] = $order->getIncrementId();
        $data['order_description'] = 'Platba za objednávku: ' . $order->getIncrementId();
        $data['items'] = $orderItems;
        $data['callback'] = array('return_url' => $this->getUrl('gopay/index/return'), 'notification_url' => $this->getUrl('gopay/index/notification'));
        $data['lang'] = ((trim($language) == '') ? 'cs' : $language);
		
		// var_dump($data);

        // Get GW URL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $gatewayUrl . 'payments/payment');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json", "Content-Type: application/json", "Authorization: Bearer " . $accessToken));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        $json = json_decode($result, true);
		
		// return $json;
		
        $gw_url = $json['gw_url'];
        curl_close($ch);

        return $gw_url;
    }

}
