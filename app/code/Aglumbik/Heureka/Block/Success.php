<?php

namespace Aglumbik\Heureka\Block;

use Magento\Framework\View\Element\Template;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Sales\Model\OrderFactory;
use Aglumbik\Heureka\Helper\Data as HeurekaHelper;

class Success extends Template {

    protected $_checkoutSession;
    protected $_productModel;
    protected $_orderFactory;
	public $_heurekaHelper;

    public function __construct(
        ProductModel     $productModel,
        Template\Context $context,
        CheckoutSession  $checkoutSession,
        OrderFactory     $orderFactory,
		HeurekaHelper    $heurekaHelper
    ) {
        parent::__construct($context);
        $this->_checkoutSession = $checkoutSession;
        $this->_productModel = $productModel;
        $this->_orderFactory = $orderFactory;
		$this->_heurekaHelper = $heurekaHelper;
    }

    public function getOrder() {
        $incrementId = $this->_checkoutSession->getLastOrderId();
        return $this->_orderFactory->create()->load($incrementId);
    }

    public function getProduct($productId) {
        return $this->_productModel->load($productId);
    }

}