<?php
namespace Aglumbik\Heureka\Model\Config\Source;

class Verze implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('CZ')], ['value' => 1, 'label' => __('SK')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('CZ'), 1 => __('SK')];
    }
}

