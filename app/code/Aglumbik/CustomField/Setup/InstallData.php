<?php
namespace Aglumbik\CustomField\Setup;
 
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
 
class InstallData implements InstallDataInterface
{
	
    protected $_salesSetupFactory;
    protected $_quoteSetupFactory;
	private $eavSetupFactory;
 
    /**
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
		EavSetupFactory $eavSetupFactory, 
		Config $eavConfig
    ) {
        $this->_salesSetupFactory = $salesSetupFactory;
        $this->_quoteSetupFactory = $quoteSetupFactory;
		$this->eavSetupFactory = $eavSetupFactory;
		$this->eavConfig = $eavConfig;
    }
 
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
 
        $salesInstaller = $this->_salesSetupFactory->create(['resourceName' => 'sales_setup','setup' => $setup]);
        $quoteInstaller = $this->_quoteSetupFactory->create(['resourceName' => 'quote_setup','setup' => $setup]);
 
        $this->addQuoteAttributes($quoteInstaller);
        $this->addOrderAttributes($salesInstaller);
		
		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY,
			'dic',
			[
				'type'         => 'varchar',
				'label'        => 'DIČ',
				'input'        => 'text',
				'required'     => false,
				'visible'      => true,
				'user_defined' => true,
				'position'     => 999,
				'system'       => 0,
			]
		);
		
		$dicAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, 'dic');
		$dicAttribute->setData('used_in_forms',['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']);
		$dicAttribute->save();
    }
 
    public function addQuoteAttributes($installer) {
        $installer->addAttribute('quote_address', 'dic', ['type' => 'text']);
    }
 
    public function addOrderAttributes($installer) {
        $installer->addAttribute('order_address', 'dic', ['type' => 'text']);
    }
}