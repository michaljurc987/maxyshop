<?php

namespace Wame\Iban\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Iban implements ObserverInterface
{

    private $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        try {
            $value = "";
            if (isset($_POST['iban'])) {
                $value = $_POST['iban'];
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerSession = $objectManager->get('Magento\Customer\Model\Session');
                $customerData = $objectManager->create('Magento\Customer\Model\Customer')->load($customerSession->getId());
                $customerData->setData('iban', $value);
                $customerData->save();
            }
        } catch (\Exception $e) {
            $this->logger->critical('Error message', ['exception' => $e]);
        }

    }
}