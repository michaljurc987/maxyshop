<?php

namespace Wame\InvoicePrint\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /* pridanie column do databazy sales_order */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'invoice_paper',
            [
                'type' => Table::TYPE_INT,
                'length' => 5000,
                'nullable' => true,
                'comment' => 'Invoice Paper'
            ]
        );

        $installer->endSetup();
    }
}
