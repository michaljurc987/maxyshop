<?php
/**
 * Created by PhpStorm.
 * User: Dinh Phuc Tran
 * Date: 29-Nov-18
 * Time: 09:41
 */

namespace Mageplaza\CustomOrderNumber\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order\Invoice;

/**
 * Class InvoiceSaveBefore
 * @package Mageplaza\CustomOrderNumber\Observer
 */
class InvoiceSaveBefore implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * InvoiceSaveBefore constructor.
     *
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->_registry = $registry;
    }

    /**
     * @param Observer $observer
     *
     * @return InvoiceSaveBefore
     */
    public function execute(Observer $observer)
    {
        /**
         * @var Invoice $invoice
         */
        $invoice = $observer->getData('invoice');
        if (!$invoice) {
            return $this;
        }
        if ($this->_registry->registry('con_new_invoice') !== null) {
            $this->_registry->unregister('con_new_invoice');
        }
        if ($invoice->isObjectNew()) {
            $this->_registry->register('con_new_invoice', $invoice);
        }

        return $this;
    }
}
