<?xml version="1.0"?>
<!--
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
 -->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <section id="customorder" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
            <class>separator-top</class>
            <label>Custom Order Number</label>
            <tab>mageplaza</tab>
            <resource>Mageplaza_CustomOrderNumber::configuration</resource>
            <group id="general" translate="label" type="text" sortOrder="1" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>General</label>
                <field id="enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Enable</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
            </group>
            <group id="order" translate="label" type="text" sortOrder="2" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>Order Number</label>
                <field id="enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Enable</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="pattern" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Pattern</label>
                    <comment><![CDATA[You can use the following variables: <br><br>- Counter: The formula is [number + counter], including the counter length and the counter.<br>&nbsp;Eg. [6counter] with the counter length being 6 and the counter being 123, it will be displayed as 000123.<br><br>- Date information: [d], [dd], [D], [m], [mm], [M], [y], [yy], [yyyy].<br>&nbsp;Eg. Monday, 1st August, 2018 is displayed as: <br><br>[d]-[m]-[y] : 1-08-18.<br>[dd]-[mm]-[yyyy]: 01-08-2018.<br>[D]-[M]-[yyyy]: MON-AUG-2018.<br><br>- [store_code]<br><br>- [store_id]<br><br>Eg. The pattern ORD-[dd][mm][yyyy][3counter]-[country_id] may result ORD-01082018123-US.]]></comment>
                </field>
                <field id="counterstart" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Start From</label>
                    <validate>validate-number validate-digits</validate>
                </field>
                <field id="counterstep" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <comment>If you enter this field as 5 and your last order has the counter as 000123, the next order of counter will be 000128.</comment>
                    <label>Counter Step</label>
                    <validate>validate-number validate-digits</validate>
                </field>
                <field id="counterstore" translate="label" type="select" sortOrder="50" showInDefault="1" showInStore="1" canRestore="1">
                    <label>Counter by Store</label>
                    <comment>Select Yes if you want to use the 'counter' variable for this store, select No to use the 'counter' variable for multiple stores.</comment>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="resetcounter" translate="label" type="select" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Reset Counter</label>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset</source_model>
                </field>
                <field id="resetnow" translate="label" sortOrder="70" showInWebsite="1" showInStore="1">
                    <frontend_model>Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config\Reset</frontend_model>
                </field>
            </group>
            <group id="invoice" translate="label" type="text" sortOrder="3" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>Invoice Number</label>
                <field id="enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Enable</label>
                    <comment>If there is more than one invoice, a subfix will be automatically added (from 2, 3..) Eg. INV-2018010212-2</comment>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Enable</source_model>
                </field>
                <field id="replace" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Replace</label>
                    <comment><![CDATA[Replace the text "ORD" with "INV" in the field "To" below.<br>Eg. The Invoice ID of an Order ID which is ORD-01-10-2005-000123 will be INV-01-10-2005-000123]]></comment>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="replaceto" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>To</label>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="pattern" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Pattern</label>
                    <comment><![CDATA[You can use the following variables: <br><br>
                                      - Counter: The formula is [number + counter], including the counter length and the counter.<br>
                                        &nbsp;Eg. [6counter] with the counter length being 6 and the counter being 123, it will be displayed as 000123.<br><br>
                                      - Date information: [d], [dd], [D], [m], [mm], [M], [y], [yy], [yyyy].<br>
                                        &nbsp;Eg. Monday, 1st August, 2018 is displayed as: <br><br>
                                        [d]-[m]-[y] : 1-08-18.<br>
                                        [dd]-[mm]-[yyyy]: 01-08-2018.<br>
                                        [D]-[M]-[yyyy]: MON-AUG-2018.<br><br>
                                      - [store_code]<br><br>
                                      - [store_id]<br><br>
                                      Eg. The pattern INV-[dd][mm][yyyy][3counter]-[country_id] may result INV-01082018123-US.
                    ]]></comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstart" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Start From</label>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstep" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Step</label>
                    <comment>If you enter this field as 5 and your last order has the counter as 000123, the next order of counter will be 000128.</comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                    <validate>validate-number validate-digits</validate>
                </field>
                <field id="counterstore" translate="label" type="select" sortOrder="70" showInDefault="1" showInStore="1" canRestore="1">
                    <label>Counter by Store</label>
                    <comment>Select Yes if you want to use the 'counter' variable for this store, select No to use the 'counter' variable for multiple stores.</comment>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                    <validate>validate-number validate-digits</validate>
                </field>
                <field id="resetcounter" translate="label" type="select" sortOrder="80" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Reset Counter</label>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="resetnow" translate="label" sortOrder="90" showInWebsite="1" showInStore="1">
                    <frontend_model>Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config\Reset</frontend_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
            </group>
            <group id="creditmemo" translate="label" type="text" sortOrder="3" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>Creditmemo Number</label>
                <field id="enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <comment>If there is more than one creditmemo, a subfix will be automatically added (from 2, 3..) Eg. CRE-2018010212-2</comment>
                    <label>Enable</label>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Enable</source_model>
                </field>
                <field id="replace" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Replace</label>
                    <comment><![CDATA[Replace the text "ORD" with "CRE" in the field "To" below.<br>Eg. The Creditmemo ID of an Order ID which is ORD-01-10-2005-000123 will be CRE-01-10-2005-000123]]></comment>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="replaceto" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>To</label>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="pattern" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Pattern</label>
                    <comment><![CDATA[You can use the following variables: <br><br>
                                      - Counter: The formula is [number + counter], including the counter length and the counter.<br>
                                        &nbsp;Eg. [6counter] with the counter length being 6 and the counter being 123, it will be displayed as 000123.<br><br>
                                      - Date information: [d], [dd], [D], [m], [mm], [M], [y], [yy], [yyyy].<br>
                                        &nbsp;Eg. Monday, 1st August, 2018 is displayed as: <br><br>
                                        [d]-[m]-[y] : 1-08-18.<br>
                                        [dd]-[mm]-[yyyy]: 01-08-2018.<br>
                                        [D]-[M]-[yyyy]: MON-AUG-2018.<br><br>
                                      - [store_code]<br><br>
                                      - [store_id]<br><br>
                                      Eg. The pattern CRE-[dd][mm][yyyy][3counter]-[country_id] may result CRE-01082018123-US.
                    ]]></comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstart" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Start From</label>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstep" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Step</label>
                    <comment>If you enter this field as 5 and your last order has the counter as 000123, the next order of counter will be 000128.</comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                    <validate>validate-number validate-digits</validate>
                </field>
                <field id="counterstore" translate="label" type="select" sortOrder="70" showInDefault="1" showInStore="1" canRestore="1">
                    <label>Counter by Store</label>
                    <comment>Select Yes if you want to use the 'counter' variable for this store, select No to use the 'counter' variable for multiple stores.</comment>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="resetcounter" translate="label" type="select" sortOrder="80" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Reset Counter</label>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="resetnow" translate="label" sortOrder="90" showInWebsite="1" showInStore="1">
                    <frontend_model>Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config\Reset</frontend_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
            </group>
            <group id="shipment" translate="label" type="text" sortOrder="3" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>Shipment Number</label>
                <field id="enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Enable</label>
                    <comment>If there is more than one shipment, a subfix will be automatically added (from 2, 3..) Eg. SHIP-2018010212-2</comment>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Enable</source_model>
                </field>
                <field id="replace" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Replace</label>
                    <comment><![CDATA[Replace the text "ORD" with "SHIP" in the field "To" below.<br>Eg. The shipment ID of an Order ID which is ORD-01-10-2005-000123 will be SHIP-01-10-2005-000123]]></comment>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="replaceto" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>To</label>
                    <depends>
                        <field id="enabled">2</field>
                    </depends>
                </field>
                <field id="pattern" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Pattern</label>
                    <comment><![CDATA[You can use the following variables: <br><br>
                                      - Counter: The formula is [number + counter], including the counter length and the counter.<br>
                                        &nbsp;Eg. [6counter] with the counter length being 6 and the counter being 123, it will be displayed as 000123.<br><br>
                                      - Date information: [d], [dd], [D], [m], [mm], [M], [y], [yy], [yyyy].<br>
                                        &nbsp;Eg. Monday, 1st August, 2018 is displayed as: <br><br>
                                        [d]-[m]-[y] : 1-08-18.<br>
                                        [dd]-[mm]-[yyyy]: 01-08-2018.<br>
                                        [D]-[M]-[yyyy]: MON-AUG-2018.<br><br>
                                      - [store_code]<br><br>
                                      - [store_id]<br><br>
                                      Eg. The pattern SHIP-[dd][mm][yyyy][3counter]-[country_id] may result SHIP-01082018123-US.
                    ]]></comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstart" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Start From</label>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="counterstep" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Counter Step</label>
                    <comment>If you enter this field as 5 and your last order has the counter as 000123, the next order of counter will be 000128.</comment>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                    <validate> validate-digits</validate>
                </field>
                <field id="counterstore" translate="label" type="select" sortOrder="70" showInDefault="1" showInStore="1" canRestore="1">
                    <label>Counter by Store</label>
                    <comment>Select Yes if you want to use the 'counter' variable for this store, select No to use the 'counter' variable for multiple stores.</comment>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="resetcounter" translate="label" type="select" sortOrder="80" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Reset Counter</label>
                    <source_model>Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset</source_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
                <field id="resetnow" translate="label" sortOrder="90" showInWebsite="1" showInStore="1">
                    <frontend_model>Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config\Reset</frontend_model>
                    <depends>
                        <field id="enabled">1</field>
                    </depends>
                </field>
            </group>
            <group id="email" translate="label" type="text" sortOrder="3" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                <label>Email</label>
                <field id="emailfrom" translate="label" type="select" sortOrder="80" showInDefault="1" canRestore="1">
                    <label>Send Email Form</label>
                    <source_model>Magento\Config\Model\Config\Source\Email\Identity</source_model>
                </field>
                <field id="emailto" translate="label comment" type="text" sortOrder="30" showInDefault="1" canRestore="1">
                    <label>Send Email To</label>
                    <validate>validate-email</validate>
                </field>
            </group>
        </section>
    </system>
</config>
