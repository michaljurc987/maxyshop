<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Reset
 * @package Mageplaza\CustomOrderNumber\Model\System\Config\Source
 */
class Reset implements ArrayInterface
{
    const RESET_DAILY   = 1;
    const RESET_WEEKLY  = 2;
    const RESET_MONTHLY = 3;
    const RESET_YEARLY  = 4;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['label' => __('No'), 'value' => ''],
            ['label' => __('Daily'), 'value' => self::RESET_DAILY],
            ['label' => __('Weekly'), 'value' => self::RESET_WEEKLY],
            ['label' => __('Monthly'), 'value' => self::RESET_MONTHLY],
            ['label' => __('Yearly'), 'value' => self::RESET_YEARLY],
        ];

        return $options;
    }
}
