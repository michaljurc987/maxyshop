<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Enable
 * @package Mageplaza\CustomOrderNumber\Model\System\Config\Source
 */
class Enable implements ArrayInterface
{
    const YES  = 1;
    const NO   = 0;
    const SAME = 2;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['label' => __('Same as Order Number'), 'value' => self::SAME],
            ['label' => __('Yes'), 'value' => self::YES],
            ['label' => __('No'), 'value' => self::NO]
        ];

        return $options;
    }
}
