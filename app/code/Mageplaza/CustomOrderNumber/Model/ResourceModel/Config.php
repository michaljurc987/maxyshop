<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Model\ResourceModel;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\CustomOrderNumber\Helper\Data as Helper;

/**
 * Class Config
 * @package Mageplaza\CustomOrderNumber\Model\ResourceModel
 */
class Config extends AbstractDb
{
    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Http
     */
    protected $_request;

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('core_config_data', 'config_id');
    }

    /**
     * Config constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param Helper $helper
     * @param Http $request
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Helper $helper,
        Http $request,
        Context $context,
        $connectionName = null
    ) {
        $this->_helper = $helper;
        $this->_request = $request;
        $this->_storeManager = $storeManager;

        parent::__construct($context, $connectionName);
    }

    /**
     * @param $scope
     * @param $scope_id
     * @param $path
     *
     * @return bool
     * @throws LocalizedException
     */
    public function getCounterValue($scope, $scope_id, $path)
    {
        $counter = $this->getConnection()
            ->select()
            ->from($this->getMainTable())
            ->where('scope_id = ?', (int) $scope_id)
            ->where('scope = ?', $scope)
            ->where('path = ?', $path)->limit(1);
        $value = $this->getConnection()->fetchRow($counter);
        if ($value) {
            return $value['value'];
        }

        return false;
    }

    /**
     * @param $path
     * @param $value
     * @param $scope
     * @param $scopeId
     *
     * @return $this
     * @throws LocalizedException
     */
    public function saveConfig($path, $value, $scope, $scopeId)
    {
        $connection = $this->getConnection();
        $select = $connection
            ->select()
            ->from($this->getMainTable())
            ->where('path = ?', $path)
            ->where('scope = ?', $scope)
            ->where('scope_id = ?', $scopeId);
        $row = $connection->fetchRow($select);

        $newData = ['scope' => $scope, 'scope_id' => $scopeId, 'path' => $path, 'value' => $value];

        $fieldFilter = $this->getIdFieldName();
        if ($row) {
            $whereCondition = [$fieldFilter . '=?' => $row[$fieldFilter]];
            $connection->update($this->getMainTable(), $newData, $whereCondition);
        } else {
            $connection->insert($this->getMainTable(), $newData);
        }

        return $this;
    }

    /**
     * @param $data
     *
     * @return array
     * @throws LocalizedException
     */
    public function resetCounter($data)
    {
        $result = [
            'success' => false,
            'message' => __('Can\'t reset in the default.')
        ];

        /*Set params*/
        $kind = $data['kind'];
        $scope = $data['scope'];
        $scopeId = $data['scopeId'];
        $path = Helper::CONFIG_MODULE_PATH . '/' . $kind . '/' . 'counter';

        /*Remove reset in default config*/
        if ($scope === 'default' && $scopeId === 0) {
            return $result;
        }

        $counterStart = 0;
        if ($kind === 'invoice') {
            $counterStart = $this->_helper->getInvoiceCounterStart($scopeId, $scope);
        }
        if ($kind === 'creditmemo') {
            $counterStart = $this->_helper->getCreditmemoCounterStart($scope, $scopeId);
        }
        if ($kind === 'shipment') {
            $counterStart = $this->_helper->getShipmentCounterStart($scopeId, $scope);
        }
        if ($kind === 'order') {
            $counterStart = $this->_helper->getOrderCounterStart($scopeId, $scope);
        }

        /*Reset counter*/
        $patch = 'customorder/' . $kind . '/counter';
        $counterNumber = $this->getCounterValue($scope, $scopeId, $patch);
        if ($counterNumber) {
            $this->saveConfig($path, $counterStart, $scope, $scopeId);
        }

        $result = [
            'success' => true,
            'message' => __('Update Success')
        ];

        return $result;
    }
}
