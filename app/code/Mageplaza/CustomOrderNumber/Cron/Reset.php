<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Cron;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface as StoreManagerInterface;
use Mageplaza\CustomOrderNumber\Helper\Data as Helper;
use Mageplaza\CustomOrderNumber\Model\ResourceModel\Config;

/**
 * Class SendEmail
 * @package Mageplaza\ReviewBooster\Cron
 */
class Reset
{
    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Reset constructor.
     *
     * @param Helper $helper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Helper $helper,
        StoreManagerInterface $storeManager
    ) {
        $this->_helper = $helper;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param $time
     */
    public function executeReset($time)
    {
        /*Set params*/
        $stores = $this->_storeManager->getStores();
        $websites = $this->_storeManager->getWebsites();

        foreach ($stores as $store) {
            foreach ($this->styleArray() as $style) {
                /** Set general params  **/
                $isCounterStore = false;
                $checkTime = false;

                switch ($style) {
                    case 'invoice':
                        $isCounterStore = $this->_helper->getInvoiceCounterStore($store->getStoreId(), 'stores');
                        $checkTime = $time == $this->_helper->getInvoiceResetCounter($store->getStoreId(), 'stores');
                        break;
                    case 'creditmemo':
                        $isCounterStore = $this->_helper->getCreditmemoCounterStore('stores', $store->getStoreId());
                        $checkTime = $time == $this->_helper->getCreditmemoResetCounter($store->getStoreId(), 'stores');
                        break;
                    case 'shipment':
                        $isCounterStore = $this->_helper->getShipmentCounterStore($store->getStoreId(), 'stores');
                        $checkTime = $time == $this->_helper->getShipmentResetCounter($store->getStoreId(), 'stores');
                        break;
                    case 'order':
                        $isCounterStore = $this->_helper->getOrderCounterStore($store->getStoreId(), 'stores');
                        $checkTime = $time == $this->_helper->getOrderResetCounter($store->getStoreId(), 'stores');
                        break;
                }
                if ($isCounterStore && $checkTime) {
                    $this->reset($style, 'stores', $store->getStoreId());
                }
            }
        }
        foreach ($websites as $website) {
            foreach ($this->styleArray() as $style) {
                $checkTime = false;
                switch ($style) {
                    case 'invoice':
                        $checkTime = $time == $this->_helper->getInvoiceResetCounter($website->getId(), 'websites');
                        break;
                    case 'creditmemo':
                        $checkTime = $time == $this->_helper->getCreditmemoResetCounter($website->getId(), 'websites');
                        break;
                    case 'shipment':
                        $checkTime = $time == $this->_helper->getShipmentResetCounter($website->getId(), 'websites');
                        break;
                    case 'order':
                        $checkTime = $time == $this->_helper->getOrderResetCounter($website->getId(), 'websites');
                        break;
                }
                if ($checkTime) {
                    $this->reset($style, 'websites', $website->getId());
                }
            }
        }
    }

    /**
     * @param $style
     * @param $storeCode
     * @param $storeId
     */
    public function reset($style, $storeCode, $storeId)
    {
        $objectManager = ObjectManager::getInstance();
        $configFactory = $objectManager->create(Config::class);
        $data = [
            'kind'    => $style,
            'scope'   => $storeCode,
            'scopeId' => $storeId
        ];

        $configFactory->resetCounter($data);
    }

    /**
     * @return array
     */
    public function styleArray()
    {
        return [
            'order',
            'invoice',
            'creditmemo',
            'shipment'
        ];
    }

    /**
     * Reset day
     */
    public function resetDay()
    {
        $this->executeReset(\Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset::RESET_DAILY);
    }

    /**
     * Reset week
     */
    public function resetWeek()
    {
        $this->executeReset(\Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset::RESET_WEEKLY);
    }

    /**
     * Reset month
     */
    public function resetMonth()
    {
        $this->executeReset(\Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset::RESET_MONTHLY);
    }

    /**
     * Reset year
     */
    public function resetYear()
    {
        $this->executeReset(\Mageplaza\CustomOrderNumber\Model\System\Config\Source\Reset::RESET_YEARLY);
    }
}
