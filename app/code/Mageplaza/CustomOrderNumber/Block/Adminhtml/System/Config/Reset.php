<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Reset
 * @package Mageplaza\CustomOrderNumber\Block\Adminhtml\System\Config
 */
class Reset extends Field
{
    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _renderInheritCheckbox(AbstractElement $element)
    {
        return '';
    }

    /**
     * Retrieve HTML markup for given form element
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $isCheckboxRequired = $this->_isInheritCheckboxRequired($element);

        // Disable element if value is inherited from other scope. Flag has to be set before the value is rendered.
        if ($element->getInherit() == 1 && $isCheckboxRequired) {
            $element->setDisabled(true);
        }

        $html = '<td class="label"><label for="' . $element->getHtmlId() . '"><span></span></label></td>';
        $html .= $this->_renderValue($element);

        if ($isCheckboxRequired) {
            $html .= $this->_renderInheritCheckbox($element);
        }

        $html .= $this->_renderHint($element);

        return $this->_decorateRowHtml($element, $html);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $data = $this->setUrlParams();
        /*Set url*/
        $url = $this->_urlBuilder->getUrl('mpcustomorder/index/reset', [
            'scope'   => $data[0],
            'scopeId' => $data[1],
            'kind'    => $element->getHtmlId()
        ]);

        return '<button type="button" class="mageplaza-reset-button" onclick="location.href=\'' . $url . '\'">' . __('Reset Now') . '</button>';
    }

    /**
     * @return array
     */
    public function setUrlParams()
    {
        $store = $this->getRequest()->getParam('store');
        $website = $this->getRequest()->getParam('website');

        /*Set params*/
        if ($store) {
            $scope = 'stores';
            $scopeId = $store;
        } elseif ($website) {
            $scope = 'websites';
            $scopeId = $website;
        } else {
            $scope = 'default';
            $scopeId = 0;
        }

        return [$scope, $scopeId];
    }
}
