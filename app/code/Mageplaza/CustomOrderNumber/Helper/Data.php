<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Helper;

use Magento\Store\Model\ScopeInterface;
use Mageplaza\Core\Helper\AbstractData as CoreHelper;

/**
 * Class Data
 * @package Mageplaza\CustomOrderNumber\Helper
 */
class Data extends CoreHelper
{
    const CONFIG_MODULE_PATH       = 'customorder';
    const ORDER_CONFIGURATION      = '/order';
    const INVOICE_CONFIGURATION    = '/invoice';
    const CREDITMEMO_CONFIGURATION = '/creditmemo';
    const SHIPMENT_CONFIGURATION   = '/shipment';

    /**
     * @param $field
     * @param null $scopeValue
     * @param string $scopeType
     *
     * @return mixed
     */
    public function getConfigValueCustom($field, $scopeValue = null, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($field, $scopeType, $scopeValue);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return bool|mixed
     */
    public function isEnabledCustom($scopeType, $storeId = null)
    {
        if (!$this->isEnabled()) {
            return false;
        }

        return $this->getConfigValueCustom(static::CONFIG_MODULE_PATH . '/general/enabled', $storeId, $scopeType);
    }
    /**
     * Order
     */

    /**
     * @param string $code
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getConfigOrder($code = '', $storeId = null, $scopeType = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValueCustom(
            static::CONFIG_MODULE_PATH . static::ORDER_CONFIGURATION . $code,
            $storeId,
            $scopeType
        );
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|bool|mixed
     */
    public function isOrderNumberEnabled($storeId = null, $scopeType = null)
    {
        if (!$this->isEnabledCustom($scopeType, $storeId)) {
            return false;
        }

        return $this->getConfigOrder('enabled', $storeId, $scopeType);
    }

    /**\
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getOrderPattern($storeId = null, $scopeType = null)
    {
        return $this->getConfigOrder('pattern', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getOrderCounterStart($storeId = null, $scopeType = null)
    {
        return $this->getConfigOrder('counterstart', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getOrderCounterStep($storeId = null, $scopeType = null)
    {
        return $this->getConfigOrder('counterstep', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getOrderCounterStore($storeId = null, $scopeType = null)
    {
        return $this->getConfigOrder('counterstore', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return mixed
     */
    public function getOrderResetCounter($storeId = null, $scopeType = null)
    {
        return $this->getConfigOrder('resetcounter', $storeId, $scopeType);
    }

    /**************************************** Invoice Config **********************************************************
     *
     * @param string $code
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getConfigInvoice($code = '', $storeId = null, $scopeType = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValueCustom(
            static::CONFIG_MODULE_PATH . static::INVOICE_CONFIGURATION . $code,
            $storeId,
            $scopeType
        );
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|bool|mixed
     */
    public function isInvoiceNumberEnabled($storeId = null, $scopeType = null)
    {
        if (!$this->isEnabledCustom($scopeType, $storeId)) {
            return false;
        }

        return $this->getConfigInvoice('enabled', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceReplace($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('replace', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceReplaceTo($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('replaceto', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoicePattern($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('pattern', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceCounterStart($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('counterstart', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceCounterStep($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('counterstep', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceCounterStore($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('counterstore', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getInvoiceResetCounter($storeId = null, $scopeType = null)
    {
        return $this->getConfigInvoice('resetcounter', $storeId, $scopeType);
    }

    /**
     * @param string $code
     * @param null $storeId
     * @param null $scopeType
     *
     * @return mixed
     */
    public function getConfigCreditmemo($code = '', $storeId = null, $scopeType = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValueCustom(
            static::CONFIG_MODULE_PATH . static::CREDITMEMO_CONFIGURATION . $code,
            $storeId,
            $scopeType
        );
    }

    /**
     * @param $scopeType
     * @param null $storeId
     *
     * @return array|bool|mixed
     */
    public function isCreditmemoNumberEnabled($scopeType, $storeId = null)
    {
        if (!$this->isEnabledCustom($scopeType, $storeId)) {
            return false;
        }

        return $this->getConfigCreditmemo('enabled', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoReplace($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('replace', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoReplaceTo($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('replaceto', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoPattern($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('pattern', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoCounterStart($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('counterstart', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoCounterStep($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('counterstep', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoCounterStore($scopeType, $storeId = null)
    {
        return $this->getConfigCreditmemo('counterstore', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getCreditmemoResetCounter($storeId = null, $scopeType = null)
    {
        return $this->getConfigCreditmemo('resetcounter', $storeId, $scopeType);
    }

    /**
     * @param string $code
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getConfigShipment($code = '', $storeId = null, $scopeType = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValueCustom(
            static::CONFIG_MODULE_PATH . static::SHIPMENT_CONFIGURATION . $code,
            $storeId,
            $scopeType
        );
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentReplace($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('replace', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentReplaceTo($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('replaceto', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|bool|mixed
     */
    public function isShipmentNumberEnabled($storeId = null, $scopeType = null)
    {
        if (!$this->isEnabledCustom($scopeType, $storeId)) {
            return false;
        }

        return $this->getConfigShipment('enabled', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentPattern($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('pattern', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentCounterStart($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('counterstart', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentCounterStep($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('counterstep', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentCounterStore($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('counterstore', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     * @param $scopeType
     *
     * @return array|mixed
     */
    public function getShipmentResetCounter($storeId = null, $scopeType = null)
    {
        return $this->getConfigShipment('resetcounter', $storeId, $scopeType);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getEmailFrom($storeId = null)
    {
        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/email/emailfrom', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getEmailTo($storeId = null)
    {
        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/email/emailto', $storeId);
    }
}
