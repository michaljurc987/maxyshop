<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Mageplaza\CustomOrderNumber\Model\ResourceModel\ConfigFactory as ConfigFactory;

/**
 * Class Reset
 * @package Mageplaza\CustomOrderNumber\Controller\Adminhtml\Index
 */
class Reset extends Action
{
    /**
     * @var ConfigFactory
     */
    protected $configFactory;

    /**
     * Reset constructor.
     *
     * @param Context $context
     * @param ConfigFactory $configFactory
     */
    public function __construct(
        Context $context,
        ConfigFactory $configFactory
    ) {
        parent::__construct($context);

        $this->configFactory = $configFactory;
    }

    /**
     * @return ResultInterface
     */
    public function execute()
    {
        /*Set param*/
        $kind = $this->getStyle();
        $scope = $this->getRequest()->getParam('scope');
        $scopeId = $this->getRequest()->getParam('scopeId');

        /*Run reset counter*/
        try {
            $data = [
                'kind'    => $kind,
                'scope'   => $scope,
                'scopeId' => $scopeId
            ];
            if ($data === null) {
                return $this->redirectPage(false, __('Something went wrong.'));
            }
            $result = $this->configFactory->create()->resetCounter($data);
            if (!$result['success']) {
                return $this->redirectPage(false, $result['message']);
            }

            return $this->redirectPage(true, $result['message']);
        } catch (Exception $e) {
            return $this->redirectPage(false, __('Something went wrong'));
        }
    }

    /**
     * @return int|string
     */
    public function getStyle()
    {
        $data = [
            'order'      => 'customorder_order_resetnow',
            'invoice'    => 'customorder_invoice_resetnow',
            'creditmemo' => 'customorder_creditmemo_resetnow',
            'shipment'   => 'customorder_shipment_resetnow'
        ];
        foreach ($data as $key => $value) {
            if ($this->getRequest()->getParam('kind') === $value) {
                return $key;
            }
        }

        return '';
    }

    /**
     * @param null $success
     * @param $message
     *
     * @return ResultInterface
     */
    public function redirectPage($success, $message)
    {
        if ($success) {
            $this->messageManager->addSuccess($message);
        } else {
            $this->messageManager->addError($message);
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getUrlConfig());

        return $resultRedirect;
    }

    /**
     * @return string
     */
    public function getUrlConfig()
    {
        $url = 'adminhtml/system_config/edit/section/customorder';
        $type = $this->getRequest()->getParam('scope');

        if ($type === 'stores') {
            $url .= '/store/';
        }
        if ($type === 'websites') {
            $url .= '/website/';
        }
        $url .= $this->getRequest()->getParam('scopeId');

        return $url;
    }
}
