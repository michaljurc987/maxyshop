<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_CustomOrderNumber
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\CustomOrderNumber\Plugin;

use Closure;
use Exception;
use Magento\Backend\Model\Session\Quote;
use Magento\Email\Model\Template\SenderResolver;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Math\Random;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\ResourceModel\Order as ResourceModel;
use Magento\SalesSequence\Model\Sequence;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\CustomOrderNumber\Helper\Data as Helper;
use Mageplaza\CustomOrderNumber\Model\ResourceModel\Config;
use Psr\Log\LoggerInterface;
use Magento\Checkout\Model\Session;

/**
 * Class Custom
 * @package Mageplaza\CustomOrderNumber\Plugin
 */
class Custom
{
    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * @type Random
     */
    protected $_mathRandom;

    /**
     * @var ConfigInterface
     */
    protected $_resourceConfig;

    /**
     * @var SenderResolver
     */
    protected $_senderResolver;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Config
     */
    protected $_configCustom;

    /**
     * @var Http
     */
    protected $_request;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var State
     */
    protected $_state;

    /**
     * @var Order
     */
    protected $_order;

    /**
     * @var Invoice
     */
    protected $_invoice;

    /**
     * @var Shipment
     */
    protected $_shipment;

    /**
     * @var Creditmemo
     */
    protected $_creditmemo;

    /**
     * @var Quote
     */
    protected $_quoteSession;

    /**
     * @var int
     */
    protected $isShipment = 0;

    /**
     * @var string
     */
    protected $scopeStore = 'stores';

    /**
     * @var string
     */
    protected $scopeWebsite = 'websites';

    /**
     * @var ResourceModel
     */
    protected $_resourceModel;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var TimezoneInterface
     */
    protected $_timezone;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var array
     */
    protected $defaultValues = [
        'pathChild'        => '',
        'scope'            => '',
        'scopeId'          => '',
        'counterStart'     => '',
        'counterStep'      => '',
        'scopeIdStore'     => '',
        'customTypeEnable' => '',
        'customPattern'    => '',
        'replace'          => '',
        'replaceTo'        => '',
        'count'            => '',
        'incrementId'      => '',
        'storeCode'        => '',
        'countryCode'      => ''
    ];

    /**
     * Custom constructor.
     *
     * @param Helper $helper
     * @param Random $mathRandom
     * @param ConfigInterface $resourceConfig
     * @param StoreManagerInterface $storeManager
     * @param Config $configCustom
     * @param Http $request
     * @param State $state
     * @param Order $order
     * @param Quote $quoteSession
     * @param Invoice $invoice
     * @param Shipment $shipment
     * @param Creditmemo $creditmemo
     * @param ResourceModel $resourceModel
     * @param TransportBuilder $transportBuilder
     * @param SenderResolver $senderResolver
     * @param LoggerInterface $logger
     * @param TimezoneInterface $timezone
     * @param Registry $registry
     * @param Session $checkoutSession
     */
    public function __construct(
        Helper $helper,
        Random $mathRandom,
        ConfigInterface $resourceConfig,
        StoreManagerInterface $storeManager,
        Config $configCustom,
        Http $request,
        State $state,
        Order $order,
        Quote $quoteSession,
        Invoice $invoice,
        Shipment $shipment,
        Creditmemo $creditmemo,
        ResourceModel $resourceModel,
        TransportBuilder $transportBuilder,
        SenderResolver $senderResolver,
        LoggerInterface $logger,
        TimezoneInterface $timezone,
        Registry $registry,
        Session $checkoutSession
    ) {
        $this->_helper           = $helper;
        $this->_mathRandom       = $mathRandom;
        $this->_resourceConfig   = $resourceConfig;
        $this->_storeManager     = $storeManager;
        $this->_request          = $request;
        $this->_configCustom     = $configCustom;
        $this->_state            = $state;
        $this->_quoteSession     = $quoteSession;
        $this->_order            = $order;
        $this->_invoice          = $invoice;
        $this->_shipment         = $shipment;
        $this->_creditmemo       = $creditmemo;
        $this->_resourceModel    = $resourceModel;
        $this->_transportBuilder = $transportBuilder;
        $this->_senderResolver   = $senderResolver;
        $this->logger            = $logger;
        $this->_timezone         = $timezone;
        $this->_registry         = $registry;
        $this->checkoutSession   = $checkoutSession;
    }

    /**
     * @return bool
     * @throws LocalizedException
     */
    public function isBackend()
    {
        return $this->_state->getAreaCode() === Area::AREA_ADMINHTML;
    }

    /**
     * @param $value
     * @param $valueFalse
     *
     * @return mixed
     */
    public function check($value, $valueFalse)
    {
        return $value ?: $valueFalse;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        /** @var Order $order */
        $orderId = $this->_request->getParam('order_id');
        $order   = $this->_order->load($orderId);
        $this->setParamsByOrder($order);

        return $order;
    }

    /**
     * @param $order
     */
    public function setParamsByOrder($order)
    {
        $this->defaultValues['incrementId']  = $order->getIncrementId();
        $this->defaultValues['scopeIdStore'] = $order->getStoreId();
        $this->defaultValues['storeCode']    = $order->getStore()->getCode();
        $this->defaultValues['storeName']    = $order->getStore()->getName();

        $address                            = $order->getShippingAddress() ?: $order->getBillingAddress();
        $this->defaultValues['countryCode'] = $address->getCountryId();
    }

    /**
     * Set params when order in frontend
     * @throws LocalizedException
     */
    public function setFrontendOrder()
    {
        $this->defaultValues['pathChild']        = 'order';
        $this->defaultValues['scopeIdStore']     = $this->_storeManager->getStore()->getId();
        $this->defaultValues['customTypeEnable'] = $this->_helper->isOrderNumberEnabled(
            $this->defaultValues['scopeIdStore'],
            $this->scopeStore
        );
        $this->defaultValues['customPattern']    = $this->_helper->getOrderPattern(
            $this->defaultValues['scopeIdStore'],
            $this->scopeStore
        );

        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $this->_storeManager->getWebsite()->getId();
        if ($this->_helper->getOrderCounterStore($this->defaultValues['scopeIdStore'], $this->scopeStore)) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getOrderCounterStart($scopeId, $scope) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getOrderCounterStep($scopeId, $scope) ?: 1;
        $this->defaultValues['storeCode']    = $this->_storeManager->getStore()->getCode();
        $this->defaultValues['storeName']    = $this->_storeManager->getStore()->getName();
        $this->defaultValues['countryCode']  = $this->checkoutSession->getQuote()->getShippingAddress()->getCountryId()
            ?: $this->checkoutSession->getQuote()->getBillingAddress()->getCountryId();
    }

    /**
     * Set params when create invoice in backend
     */
    public function setBackendInvoice()
    {
        $this->defaultValues['pathChild']        = 'invoice';
        $order                                   = $this->getOrder();
        $scopeIdWebsite                          = $order->getStore()->getWebsiteId();
        $storeScope                              = $this->defaultValues['scopeIdStore'];
        $this->defaultValues['customTypeEnable'] = $this->_helper->isInvoiceNumberEnabled(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['customPattern']    = $this->_helper->getInvoicePattern($storeScope, $this->scopeStore);
        $counterStore                            = $this->_helper->getInvoiceCounterStore(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['replace']          = $this->_helper->getInvoiceReplace($storeScope, $this->scopeStore);
        $this->defaultValues['replaceTo']        = $this->_helper->getInvoiceReplaceTo($storeScope, $this->scopeStore);
        $this->defaultValues['count']            = count($order->getInvoiceCollection()->getAllIds());
        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $scopeIdWebsite;
        if ($counterStore) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getInvoiceCounterStart($scopeId, $scope) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getInvoiceCounterStep($scopeId, $scope) ?: 1;
    }

    /**
     * Set params when create invoice in frontend
     *
     * @param Invoice $invoice
     */
    public function setFrontendInvoice(Invoice $invoice)
    {
        $this->defaultValues['pathChild']        = 'invoice';
        $order                                   = $invoice->getOrder();
        $scopeIdWebsite                          = $order->getStore()->getWebsiteId();
        $storeScope                              = $this->defaultValues['scopeIdStore'];
        $this->defaultValues['customTypeEnable'] = $this->_helper->isInvoiceNumberEnabled(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['customPattern']    = $this->_helper->getInvoicePattern($storeScope, $this->scopeStore);
        $counterStore                            = $this->_helper->getInvoiceCounterStore(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['replace']          = $this->_helper->getInvoiceReplace($storeScope, $this->scopeStore);
        $this->defaultValues['replaceTo']        = $this->_helper->getInvoiceReplaceTo($storeScope, $this->scopeStore);
        $this->defaultValues['count']            = (int) 0;
        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $scopeIdWebsite;
        if ($counterStore) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getInvoiceCounterStart($scopeId, $scope) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getInvoiceCounterStep($scopeId, $scope) ?: 1;
    }

    /**
     * Set params when create shipment in backend
     */
    public function setBackendShipment()
    {
        $this->defaultValues['pathChild']        = 'shipment';
        $order                                   = $this->getOrder();
        $scopeIdWebsite                          = $order->getStore()->getWebsiteId();
        $storeScope                              = $this->defaultValues['scopeIdStore'];
        $this->defaultValues['customTypeEnable'] = $this->_helper->isShipmentNumberEnabled(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['customPattern']    = $this->_helper->getShipmentPattern($storeScope, $this->scopeStore);
        $counterStore                            = $this->_helper->getShipmentCounterStore(
            $storeScope,
            $this->scopeStore
        );
        $this->defaultValues['replace']          = $this->_helper->getShipmentReplace($storeScope, $this->scopeStore);
        $this->defaultValues['replaceTo']        = $this->_helper->getShipmentReplaceTo($storeScope, $this->scopeStore);
        $this->defaultValues['count']            = count($order->getShipmentsCollection()->getAllIds());
        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $scopeIdWebsite;
        if ($counterStore) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getShipmentCounterStart($scopeId, $scope) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getShipmentCounterStep($scopeId, $scope) ?: 1;
    }

    /**
     * Set params when create Creditmemo in backend
     */
    public function setBackendCreditmemo()
    {
        $this->defaultValues['pathChild']        = 'creditmemo';
        $order                                   = $this->getOrder();
        $scopeIdWebsite                          = $order->getStore()->getWebsiteId();
        $storeScope                              = $this->defaultValues['scopeIdStore'];
        $this->defaultValues['customTypeEnable'] = $this->_helper->isCreditmemoNumberEnabled(
            $this->scopeStore,
            $storeScope
        );
        $this->defaultValues['customPattern']    = $this->_helper->getCreditmemoPattern($this->scopeStore, $storeScope);
        $counterStore                            = $this->_helper->getCreditmemoCounterStore(
            $this->scopeStore,
            $storeScope
        );
        $this->defaultValues['replace']          = $this->_helper->getCreditmemoReplace($this->scopeStore, $storeScope);
        $this->defaultValues['replaceTo']        = $this->_helper->getCreditmemoReplaceTo(
            $this->scopeStore,
            $storeScope
        );
        $this->defaultValues['count']            = count($order->getCreditmemosCollection()->getAllIds());
        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $scopeIdWebsite;
        if ($counterStore) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getCreditmemoCounterStart($scope, $scopeId) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getCreditmemoCounterStep($scope, $scopeId) ?: 1;
    }

    /**
     * Set params when create order in backend
     */
    public function setBackendOrder()
    {
        $this->defaultValues['pathChild']        = 'order';
        $scopeIdWebsite                          = $this->_quoteSession->getQuote()->getStore()->getWebsiteId();
        $this->defaultValues['scopeIdStore']     = $this->_quoteSession->getQuote()->getStoreId();
        $storeScope                              = $this->defaultValues['scopeIdStore'];
        $this->defaultValues['customTypeEnable'] = $this->_helper->isOrderNumberEnabled($storeScope, $this->scopeStore);
        $this->defaultValues['customPattern']    = $this->_helper->getOrderPattern($storeScope, $this->scopeStore);
        $counterStore                            = $this->_helper->getOrderCounterStore($storeScope, $this->scopeStore);
        /** If this store select get counter of website **/
        $scope   = $this->scopeWebsite;
        $scopeId = $scopeIdWebsite;
        if ($counterStore) {
            $scope   = $this->scopeStore;
            $scopeId = $this->defaultValues['scopeIdStore'];
        }
        $this->defaultValues['scope']        = $scope;
        $this->defaultValues['scopeId']      = $scopeId;
        $this->defaultValues['counterStart'] = $this->_helper->getOrderCounterStart($scopeId, $scope) ?: 0;
        $this->defaultValues['counterStep']  = $this->_helper->getOrderCounterStep($scopeId, $scope) ?: 1;
        $this->defaultValues['storeCode']    = $this->_quoteSession->getQuote()->getStore()->getCode();
        $this->defaultValues['storeName']    = $this->_quoteSession->getQuote()->getStore()->getName();
        $this->defaultValues['countryCode']  = $this->_quoteSession->getQuote()->getShippingAddress()->getCountryId()
            ?: $this->_quoteSession->getQuote()->getBillingAddress()->getCountryId();
    }

    /**
     * @param $customerOrderNumber
     *
     * @return mixed
     */
    public function processPatternOrder($customerOrderNumber)
    {
        if ($this->defaultValues['customTypeEnable'] && $this->defaultValues['customPattern'] === null) {
            return $customerOrderNumber;
        }
        if ($this->defaultValues['customTypeEnable']) {
            $customerOrderNumber = $this->generateCode($this->defaultValues['customPattern']);
        }

        return $customerOrderNumber;
    }

    /**
     * @param $customerOrderNumber
     *
     * @return mixed|string
     * @throws LocalizedException
     */
    public function processPattern($customerOrderNumber)
    {
        //frontend and order in admin
        if (!$this->isBackend() || ($this->isBackend() && $this->_request->getPost('order'))) {
            if ($this->_registry->registry('con_new_invoice')) {
                switch ($this->defaultValues['customTypeEnable']) {
                    case 1:
                        $code                = $this->generateCode($this->defaultValues['customPattern']);
                        $customerOrderNumber = $this->incrementIdExistBackend($code);

                        return $customerOrderNumber;
                    case 2:
                        if ($this->defaultValues['replace'] === null) {
                            return $this->incrementIdExist($this->processPatternOrder($customerOrderNumber));
                        }
                        /**
                         * @var Invoice $invoice
                         */
                        $invoice             = $this->_registry->registry('con_new_invoice');
                        $customerOrderNumber = $invoice->getOrder()->getIncrementId();
                        if ($this->defaultValues['replaceTo']) {
                            $replaceTo           = $this->defaultValues['replaceTo'];
                            $customerOrderNumber = str_replace(
                                $this->defaultValues['replace'],
                                $replaceTo,
                                $customerOrderNumber
                            );
                        }

                        return $customerOrderNumber;
                    default:
                        return $customerOrderNumber;
                }
            }

            return $this->incrementIdExist($this->processPatternOrder($customerOrderNumber));
        }

        /*Other types in admin: invoice, shipment, creditmemo*/
        switch ($this->defaultValues['customTypeEnable']) {
            case 1:
                $code                = $this->generateCode($this->defaultValues['customPattern']);
                $customerOrderNumber = $this->incrementIdExistBackend($code);
                break;
            case 2:
                if ($this->defaultValues['replace'] === null) {
                    return $customerOrderNumber;
                }

                $count               = $this->defaultValues['count'];
                $customerOrderNumber = $this->defaultValues['incrementId'];
                if ($count) {
                    $customerOrderNumber .= '-' . $count;
                }
                $replaceTo = $this->defaultValues['replaceTo'];
                if ($replaceTo) {
                    $customerOrderNumber = str_replace(
                        $this->defaultValues['replace'],
                        $replaceTo,
                        $customerOrderNumber
                    );
                }
                break;
            default:
                break;
        }

        return $customerOrderNumber;
    }

    /**
     * @param $customerOrderNumber
     *
     * @return string
     * @throws LocalizedException
     */
    public function incrementIdExistBackend($customerOrderNumber)
    {
        $pool         = Random::CHARS_UPPERS . Random::CHARS_DIGITS;
        $rand         = $this->_mathRandom->getRandomString(6, $pool);
        $beforeChange = $customerOrderNumber;
        switch ($this->defaultValues['pathChild']) {
            case 'invoice':
                if ($this->_invoice->loadByIncrementId($customerOrderNumber)->getId()) {
                    $customerOrderNumber = $customerOrderNumber . '-' . $rand;
                }
                break;
            case 'shipment':
                if ($this->_shipment->loadByIncrementId($customerOrderNumber)->getId()) {
                    $customerOrderNumber = $customerOrderNumber . '-' . $rand;
                }
                break;
            case 'creditmemo':
                $creditmemoIds = $this->_creditmemo->getCollection()
                    ->addAttributeToFilter('increment_id', $customerOrderNumber)
                    ->getAllIds();
                if ($creditmemoIds) {
                    $customerOrderNumber = $customerOrderNumber . '-' . $rand;
                }
                break;
        }
        $this->sendMail($beforeChange, $customerOrderNumber);

        return $customerOrderNumber;
    }

    /**
     * Check order id is exist
     *
     * @param $customerOrderNumber
     *
     * @return string
     * @throws LocalizedException
     */
    public function incrementIdExist($customerOrderNumber)
    {
        $pool         = Random::CHARS_UPPERS . Random::CHARS_DIGITS;
        $beforeChange = $customerOrderNumber;

        $connection = $this->_resourceModel->getConnection();
        $select     = $connection->select()
            ->from($this->_resourceModel->getMainTable())
            ->where('increment_id' . ' = ' . '"' . $customerOrderNumber . '"');
        $result     = $connection->fetchOne($select);

        if ($result) {
            $customerOrderNumber = $customerOrderNumber . '-' . $this->_mathRandom->getRandomString(6, $pool);
            $this->sendMail($beforeChange, $customerOrderNumber);
        }

        return $customerOrderNumber;
    }

    /**
     * @param Sequence $subject
     * @param Closure $proceed
     *
     * @return mixed|string
     * @throws LocalizedException
     */
    public function aroundGetCurrentValue(Sequence $subject, Closure $proceed)
    {
        $customerOrderNumber = $proceed();

        if ($this->isBackend()) {
            if ($this->_request->getPost('invoice')) {
                if ($this->isShipment == 1) {
                    $this->setBackendShipment();
                } else {
                    $this->setBackendInvoice();
                }
                $data = $this->_request->getPost('invoice');
                if (!empty($data['do_shipment'])) {
                    $this->isShipment = 1;
                }
            }
            if ($this->_request->getPost('shipment')) {
                $this->setBackendShipment();
            }
            if ($this->_request->getPost('creditmemo')) {
                $this->setBackendCreditmemo();
            }
            if ($this->_request->getPost('order')) {
                $this->setBackendOrder();
            }
        } else {
            $this->setFrontendOrder();
            if ($this->_registry->registry('con_new_invoice')) {
                $invoice = $this->_registry->registry('con_new_invoice');
                $this->setFrontendInvoice($invoice);
            }
        }

        return $this->processPattern($customerOrderNumber);
    }

    /**
     * @param null $pattern
     *
     * @return mixed
     */
    public function generateCode($pattern = null)
    {
        /*Set params*/
        $code          = $pattern = str_replace(' ', '', $pattern);
        $patternString = '#\[(.{0}|[0-9]+)([ANDMcounterdmysi_]{1,10})\]#';

        $configTimezone = $this->_timezone->getConfigTimezone();
        date_default_timezone_set($configTimezone);

        /*Process pattern*/
        if (preg_match($patternString, $pattern)) {
            $code = preg_replace_callback(
                $patternString,
                function ($param) {
                    if ($param[1] == null) {
                        switch ($param[2]) {
                            case 'store_code':
                                return strtoupper($this->defaultValues['storeCode']);
                            case 'store_id':
                                return $this->defaultValues['scopeIdStore'];
                            case 'country_id':
                                return $this->defaultValues['countryCode'];
                            case 'y':
                                return date('y');
                            case 'yy':
                                $patternDate = "%'.02d";

                                return sprintf($patternDate, (int) date('y'));
                            case 'yyy':
                            case 'yyyy':
                                return date('Y');
                            case 'd':
                                return date('d');
                            case 'dd':
                                $patternDate = "%'.02d";

                                return sprintf($patternDate, (int) date('d'));
                            case 'D':
                                return date('D');
                            case 'm':
                                return date('m');
                            case 'mm':
                                $patternDate = "%'.02d";

                                return sprintf($patternDate, (int) date('m'));
                            case 'M':
                                return date('M');
                        }
                    } else {
                        switch ($param[2]) {
                            case 'counter':
                                $counterNumber = $this->generateCounter();
                                /** Set length counter */
                                $counterPattern = "%'.0" . $param[1] . "d";
                                $counter        = sprintf(
                                    $counterPattern,
                                    $counterNumber
                                );

                                return $counter;
                            case 'A':
                                $pool = Random::CHARS_UPPERS;

                                return $this->_mathRandom->getRandomString($param[1], $pool);
                            case 'N':
                                $pool = Random::CHARS_DIGITS;

                                return $this->_mathRandom->getRandomString($param[1], $pool);
                            case 'AN':
                                $pool = Random::CHARS_UPPERS . Random::CHARS_DIGITS;

                                return $this->_mathRandom->getRandomString($param[1], $pool);
                        }
                    }
                },
                $pattern
            );
        }

        return $code;
    }

    /**
     * @param $path
     * @param $value
     * @param $scope
     * @param $scope_id
     *
     * @throws LocalizedException
     */
    public function saveCounterValue($path, $value, $scope, $scope_id)
    {
        $this->_configCustom->saveConfig(
            Helper::CONFIG_MODULE_PATH . '/' . $path . '/' . 'counter',
            $value,
            $scope,
            $scope_id
        );
    }

    /**
     * @return bool|int|mixed
     * @throws LocalizedException
     */
    public function generateCounter()
    {
        $patch = 'customorder/' . $this->defaultValues['pathChild'] . '/counter';

        /** Get|Set value counter */
        $counterNumber = $this->_configCustom->getCounterValue(
            $this->defaultValues['scope'],
            $this->defaultValues['scopeId'],
            $patch
        );
        if ($counterNumber === false) {
            $counterNumber = $this->defaultValues['counterStart'];
        } else {
            $counterNumberNext = (int) $counterNumber + (int) $this->defaultValues['counterStep'];
            $counterNumber     = $counterNumberNext;
        }
        $this->saveCounterValue(
            $this->defaultValues['pathChild'],
            $counterNumber,
            $this->defaultValues['scope'],
            $this->defaultValues['scopeId']
        );

        return $counterNumber;
    }

    /**
     * @param $beforeChange
     * @param $customerOrderNumber
     */
    private function sendMail($beforeChange, $customerOrderNumber)
    {
        try {
            $from = $this->_senderResolver->resolve($this->_helper->getEmailFrom() ?: 'general');

            $this->_transportBuilder->setTemplateIdentifier('mageplaza_fail_number_template')
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $this->defaultValues['scopeIdStore']])
                ->setTemplateVars([
                    'type'         => ucwords($this->defaultValues['pathChild']),
                    'idIncre'      => $customerOrderNumber,
                    'storeName'    => $this->defaultValues['storeName'],
                    'storeCode'    => $this->defaultValues['storeCode'],
                    'beforeChange' => $beforeChange
                ])
                ->setFrom($from)
                ->addTo($this->_helper->getEmailTo() ?: '')
                ->getTransport()
                ->sendMessage();
        } catch (Exception $e) {
            $this->logger->critical($e);
        }
    }
}
