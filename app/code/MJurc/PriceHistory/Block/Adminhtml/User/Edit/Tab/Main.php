<?php
namespace MJurc\PriceHistory\Block\Adminhtml\User\Edit\Tab;

use Magento\Backend\Block\Widget\Form;

class Main extends \Magento\User\Block\User\Edit\Tab\Main
{
    /**
     * Prepare form fields
     *
     * @return Form
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $model = $this->_coreRegistry->registry('permissions_user');
        $baseFieldset = $form->getElement('base_fieldset');
        $baseFieldset->addField(
            'pricehistory',
            'text',
            [
                'name' => 'price_history',
                'label' => __('Price History'),
                'title' => __('Price History'),
                'value' => $model->getPriceHistory(),
                'note' => __('0 - Price History is disabled for user, 1 - Price History is enabled for user')
            ]
        );
        return $this;
    }
}