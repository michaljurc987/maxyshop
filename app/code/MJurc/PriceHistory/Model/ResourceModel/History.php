<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace MJurc\PriceHistory\Model\ResourceModel;

class History extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $extensionUser = $objectManager->get('Magento\Backend\Model\Auth\Session')->getUser()->getPriceHistory();
        if($extensionUser == 1) {
            $this->_init('history_price', 'id');
        } else {
            exit;
        }
    }

    /**
     * @param $data
     * @return $this
     */
    public function bulkPriceChangeInsert($data)
    {
        $this->getConnection()->insertMultiple($this->getMainTable(), $data);

        return $this;
    }
}
